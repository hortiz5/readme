# README

_Under construction._ 🚧 😬

<!-- TODO: Add information about me. Look at these resources and other GitLab team members' READMEs for inspiration 😃:

  - [Creating and publishing your GitLab README](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/#creating-and-publishing-your-gitlab-readme )
  - [12 “Manager READMEs” from Silicon Valley’s Top Tech Companies | Hacker Noon](https://hackernoon.com/12-manager-readmes-from-silicon-valleys-top-tech-companies-26588a660afe) -->

## Work Log

<!-- TODO: Should probably move this to another doc or project. -->

### Week 30 (July 26)

#### 2021-07-26

1. Complete onboarding tasks:
   1. [x] Do harassment prevention training.
      >Please complete the [Harassment Prevention Training](https://about.gitlab.com/handbook/people-group/learning-and-development/#compliance-courses) via WILL Learning within your first 30 days. Instructions can be found under the *Common Ground: Harassment Prevention Training* section. You can find the link to the appropriate course in BambooHR (for detailed instructions [see here](https://about.gitlab.com/handbook/people-group/learning-and-development/compliance-courses/#will-interactive)).
   1. [x] Read about people policies.
      >Please read our [People Policies](https://about.gitlab.com/handbook/people-policies) handbook page which contains important team member-related policies to ensure that we adhere with all required laws.

#### 2021-07-27

1. Complete onboarding tasks:
   1. [x] Attend CEO calls (there are actually two calls happening today; one for presentations, and one as an AMA).
      >[CEO 101 call](https://about.gitlab.com/culture/gitlab-101/). Within the calendar description, you will find a link to the call agenda where all team members are able to insert questions for the CEO to answer in the session. Tip: There are videos of previous CEO 101 sessions within the GitLab archives that may help you determine the question you would like to contribute.
   1. [x] Read about `#allremote` onboarding.
      >Learn more about the `#allremote` onboarding process and additional [best practices](https://about.gitlab.com/company/culture/all-remote/learning-and-development/#how-do-you-onboard-new-team-members).
   1. [x] Improve the handbook: Have [Draft: Improve instructions to edit the website locally (!86737)](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/86737) ready for review.
1. [x] Complete [Regulation FD training](https://gitlab.edcast.com/pathways/gitlab-regulation-fd-training).
1. [x] Follow up on delivery of work laptop (stuck in 🇲🇽 customs).
   - Marc DiSabatino received an email from 🇲🇽 customs saying the package is worth more than USD $1,000 and needs to be imported. We are not sure how that would be done yet.
1. [x] Investigate list items styling issue in the handbook.
   - Reported it [in Slack 🧵](https://gitlab.slack.com/archives/C02PF508L/p1627434472464800), and it has been fixed by [!87218](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/87218) now!

#### 2021-07-28

1. [General onboarding](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2845):
   1. [ ] ~~Complete~~ the [Remote Work Foundations Certification](https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge).
      - Had some progress, but I underestimated how much reading is required and I'm finding myself not being sure if I already read something or not :sweat_smile: . In particular, the handbook page on async communication is huge, and I've found stuff keeps getting added. Not bad, but I think some pages will need to have highlights at the top in the same way we recommend this for MRs with long descriptions :grimacing: .
   1. [x] Complete [Communication Certification](https://docs.google.com/forms/d/e/1FAIpQLScyyIQDdC3oN-H6-IJJM1QlZNHaGPI0jESb9ogAfkQlMzKgwQ/viewform).
1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. [x] Assign to myself one of the issues in the [Package onboarding issue](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5) and start looking into it.
      - Grabbed [GitLab #330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) and looked at the related MR and discussion for context and to understand the problem and proposed fix.
1. Other:
   1. [x] 1-on-1 with Dan.
   1. [x] Followed up on work laptop delivery. It seems I'll need to follow up with a customs agency to import the item :money_with_wings: .

#### 2021-07-29

1. [General onboarding](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2845):
   1. [x] Complete the [Remote Work Foundations Certification](https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge).
   1. [x] Become aware of the [materials used by our professional services team](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/offerings/#training--education) to train our customers.
   1. Become familiar with GitLab. Familiarize with the dashboard, the projects, and the issue tracker. Become familiar with the `README.md`s of the main projects:
      1. [x] [GitLab www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/README.md)
      1. [x] [GitLab](https://gitlab.com/gitlab-org/gitlab/blob/master/README.md)
      1. [x] [GitLab FOSS Edition](https://gitlab.com/gitlab-org/gitlab-foss/blob/master/README.md) (read-only mirror of GitLab with proprietary software removed; look at [#13304](https://gitlab.com/gitlab-org/gitlab/-/issues/13304) for more info)
1. Other:
   1. [x] Sync with Steve.
   1. [ ] Follow up on work laptop import 🇺🇸 ➡️ 🇲🇽 .

#### 2021-07-30

1. [General onboarding](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2845):
   1. [x] Learn about GitLab's Value Framework.
      >GitLab's Value Framework is foundational to GitLab's go-to-market messaging. We want to ensure all GitLab team members (even those outside of sales) are aware of what it is, why it is so important to our continued growth and success, and what it means to our customers, partners, and GitLab team members. Learn more in [this virtual session recording](http://bit.ly/37FqLC4) and the [Command of the Message](https://about.gitlab.com/handbook/sales/command-of-the-message/) handbook page.
   1. [x] Learn about GitLab's customers.
      >Take a look at GitLab's [public customer list](https://about.gitlab.com/customers/) to learn about our list of advertised customers.
   1. [x] Learn about GitLab's product tiers.
      >Take a look at GitLab's [product tier use-cases](https://about.gitlab.com/handbook/ceo/pricing/#three-tiers) and [product marketing tiers](https://about.gitlab.com/handbook/marketing/product-marketing/tiers/) to learn about GitLab product capabilities, tiers, and the motivation behind each tier.
   1. [ ] Complete onboarding survey and read about OSAT in the handbook.
      >Set a reminder for 30 days past your start date to complete the [Onboarding Survey](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSdU8qxThxRu3tdrExFGBJ1GlyucCoJj2kDpcvOlM4AiRGOavQ/viewform) and kindly review [the OSAT](https://about.gitlab.com/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat). It is vitally important for us to continue getting feedback and iterate on onboarding.
   1. [x] Set reminder to add a review in Glassdoor or Comparably.
      >Help spread the word about life at GitLab. Set a reminder in sixty days to share your feedback by leaving a review on [Glassdoor](https://www.glassdoor.com/Overview/Working-at-GitLab-EI_IE1296544.11,17.htm) or [Comparably](https://www.comparably.com/companies/gitlab). We welcome positive and negative feedback.
1. Other:
   1. [ ] Follow up on work laptop import 🇺🇸 ➡️ 🇲🇽 .

### Week 31 (August 2)

#### 2021-08-02

1. [General onboarding](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2845):
   1. [x] Complete onboarding survey and read about OSAT in the handbook.
      >Set a reminder for 30 days past your start date to complete the [Onboarding Survey](https://docs.google.com/a/gitlab.com/forms/d/e/1FAIpQLSdU8qxThxRu3tdrExFGBJ1GlyucCoJj2kDpcvOlM4AiRGOavQ/viewform) and kindly review [the OSAT](https://about.gitlab.com/handbook/people-group/people-group-metrics/#onboarding-satisfaction-osat). It is vitally important for us to continue getting feedback and iterate on onboarding.
   1. [x] Familiarize with [Database Lab Engine](https://docs.gitlab.com/ee/development/understanding_explain_plans.html#database-lab-engine)
      >Familiarize yourself with the [`#database-lab`](https://docs.gitlab.com/ee/development/understanding_explain_plans.html#database-lab). This can be helpful to verify query plans and execute DDL statements in a clone of the production database.
      - I checked the provided links, the [postgres.ai](postgres.ai) site (signing up with my `@gitlab.com` Google account), the [Database Lab Engine](https://gitlab.com/postgres-ai/database-lab) project in GitLab, and the [Database Lab in 2 Minutes video](https://youtu.be/IZzGJa2P3lU). I found a few Slack channels (`#database-lab`, `#postgres-ai-gitlab`, `#g_database`) that led me to a [Database Lab Overview](https://youtu.be/4lpVM6tPYg4) video. I haven't requested access to the GitLab organization in postgres.ai yet, but will do when I need it.
1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. [ ] Start local dev environment setup for [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab) to be able to tackle [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
1. Other:
   1. [x] Follow up on work laptop import 🇺🇸 ➡️ 🇲🇽 .
      - No luck calling customs agencies and from talking to DHL support I'm afraid I may not be able to import the laptop just like an end user / consumer. However, we will order the laptop from the Mexican Dell store instead. Apparently, it wasn't known that Dell has an online store for 🇲🇽, and that was the reason it was bought in 🇺🇸. I had thought there was a different reason since I've long known about the 🇲🇽 store and even bought a monitor there last year 😓.

#### 2021-08-03

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. [x] Start local dev environment setup for [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab) to be able to tackle [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
      - Started going through the GitLab project README and installation docs there. Learned about the dozens of ways to use GitLab, the different ways to install a self-managed instance on premises or using cloud providers, the dozens of partners, client apps, spin-offs and integrations. GitLab is a big monster (in a good way 😄).
   1. [x] Look at [gitlab-org&5070](https://gitlab.com/groups/gitlab-org/-/epics/5070) and [gitlab-org#337446](https://gitlab.com/gitlab-org/gitlab/-/issues/337446) and learn a bit about Helm packages.
1. Other:
   1. [x] Sync up with Tim.
   1. [x] Follow up on work laptop import 🇺🇸 ➡️ 🇲🇽 .
      - Marc tried ordering the laptop in the 🇲🇽 Dell store. He had an issue and I'm not sure if he was able to complete the purchase.
      - I'm also following up with the customs notifier that contacted Marc via email.

#### 2021-08-04

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. [x] Work on local dev environment setup for [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab) to be able to tackle [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
      - Finished going through the GitLab project README, and the installation and other related docs.
      - Looked at [gitlab-org/gitlab-development-kit](https://gitlab.com/gitlab-org/gitlab-development-kit) docs. Will need to follow the guide to [Migrate from self-managed dependencies to GDK-managed dependencies using `asdf`](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/migrate_to_asdf.md) since I had manually set up Ruby with rbenv, Node with NVM and Yarn for the gitlab-com/www-gitlab-com project 🙂.
1. Other:
   1. [x] 1-on-1 with Dan.
   1. [x] Follow up on getting my work laptop.
      - Marc has had a couple issues ordering the laptop in the 🇲🇽 Dell store. But that's still the path we are going down right now. Need to confirm with him if he was finally able to finish the purchase.

#### 2021-08-05

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. [x] Work on local dev environment setup for [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab) to be able to tackle [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
      - Looked at the guide to [Migrate from self-managed dependencies to GDK-managed dependencies using `asdf`](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/migrate_to_asdf.md) and removed my rbenv, NVM and Yarn installations.
      - Installed GDK using the [one-line installation](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/index.md#one-line-installation) script.
1. Other:
   1. [x] Sync with Steve.
   1. [x] Coffee-chat with Brad Sevy.
   1. [x] Caught up on Slack channels to keep gaining context and discover things I don't know about and may eventually need to learn about (have been doing this more on a daily basis this week).
   1. [x] Did some exploration in gitlab.com and played a bit with search / filter / sort features. Tried the My-reaction filter after I learned how it came to be by searching for issues and MRs related to bookmarking / favoriting / starring stuff. I did find a some good information and discussions related to that functionality. So, I tried that filter as a way of bookmarking issues, epics, and MRs to come back to them when I have more bandwith. David had suggested that method to me a few weeks ago. So, I finally decided to try it and it works like charm 👌. I went to a few objects I had _marked_ using To-Dos and marked them with a 🎗️ instead 😃.

#### 2021-08-06

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. [x] Work on local dev environment setup for [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab) to be able to tackle [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
      - I continued reading the GDK docs, but didn't have much additional progress. I was happy setting up GDK was easy and `gdk doctor` reported a healthy installation, and I then allowed myself to be distracted with other things.
   1. [x] Have a look at the **Important Links** in the [Package: Weekly Team Agenda (2021) doc](https://docs.google.com/document/d/1p5kOeIAqeZI1sViB6zPsONspN8dsCHeP86_p5yNydnU/edit#) and plan to talk about some of the linked information on sync calls with teammates.
      - I learned it is another option for setting up GitLab locally for development, using Docker containers for the different components of the GitLab architecture. It seems to be a good option if using Linux (because Docker is more viable / performant here), providing more isolation, requiring less maintenance, and having less fragility compared to the GDK. It'd require more resources though 🤔. Anyway, something I'd like to try eventually.
1. Other:
   1. [x] Learn a bit about the [GCK](https://gitlab.com/gitlab-org/gitlab-compose-kit).
   1. [x] Make [a list of topics for sync meetings](#topics-for-sync-meetings) so that I don't forget to ask about things I'm interested in.
   1. [ ] Add planned PTO in **Upcoming PTO Calendar** table in the [Package: Weekly Team Agenda (2021) doc](https://docs.google.com/document/d/1p5kOeIAqeZI1sViB6zPsONspN8dsCHeP86_p5yNydnU/edit#).

### Week 32 (August 9)

#### 2021-08-09

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. [x] Work on local dev environment setup for [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab) to be able to tackle [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
      - Continued reading the GDK docs. Explored most of the [Basic](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/index.md#basic) and [Tips and Tricks](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/index.md#tips-and-tricks) topics in the [How to use GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/index.md) guide. As per my sync with David, the most important things to set up / run for Package would be: GitLab Rails, Local Network Address Binding. Eventually I may need the Runner. And depending on what I'm working on, I may need the Container Registry or Object Storage.
      - Played a bit with several GDK commands.
1. Other:
   1. [x] Add planned PTO in **Upcoming PTO Calendar** table in the [Package: Weekly Team Agenda (2021) doc](https://docs.google.com/document/d/1p5kOeIAqeZI1sViB6zPsONspN8dsCHeP86_p5yNydnU/edit#).
   1. [x] Sync with David.

#### 2021-08-10

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. [x] Work on local dev environment setup for [gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab).
      - Set up [Local Network Binding](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/local_network.md).
   1. [x] Start working on [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
      - Started working on what will be the first MR: fixing the `push_package` event track call for NPM packages. As per David's suggestion, I'll submit an MR per package type. I did the suggested 1-line fix and have been familiarizing myself with the API endpoint code and unit tests. I found the relevant tests that need to be adapted or where new ones will need to be added.

#### 2021-08-11

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. [x] Continue working on [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
      - Run and debugged the unit tests for the NPM packages PUT endpoint.
      - Had a closer look at the code to get more familiar with the error handling and validations.
      - Added a red test (as an RSpec shared example) that confirms the existence of the bug.
      - Did the 1-line fix at the correct spot in the endpoint logic and made the test pass. Included the shared example for a couple test cases (RSpec contexts), but still need to add it for more cases.
      - Submitted a [draft MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054/diffs) with current progress.
1. Other:
   1. [x] 1-on-1 with Dan.

#### 2021-08-12

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. Continue working on [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
      - [x] Fix rubocop offenses.
      - [x] Test that the `push_package` event is not created for other failure cases in the existing tests.
      - [ ] Learn how to test the fix manually in local environment?
   1. Start reading some of the following docs. It's important to do this as I'm working on my first Package MR:
      - [ ] [Package Group](https://about.gitlab.com/handbook/engineering/development/ops/package/)
      - [ ] [Packages Development](https://docs.gitlab.com/ee/development/packages.html)
      - [ ] [doc/api/packages/npm.md · master · GitLab.org / GitLab](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/api/packages/npm.md)
      - [ ] [Snowplow Guide](https://docs.gitlab.com/ee/development/snowplow/)
      - [ ] [Style guides](https://docs.gitlab.com/ee/development/contributing/style_guides.html)
      - [ ] [Testing standards and style guidelines](https://docs.gitlab.com/ee/development/testing_guide/index.html)
      - [ ] [Testing best practices](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html)
      - [ ] [Engineering Workflow](https://about.gitlab.com/handbook/engineering/workflow/)
      - [ ] [Issues workflow](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html)
      - [ ] [Merge requests workflow](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html)
      - [ ] [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html)
1. Other:
   1. [x] Catch up on list of Slack channels I'm following. I've been reducing the list, but it can still take a good amount of time when I leave this for the end of the week. Still, it's important for me to keep getting context and learn the _things that I don't know I don't know_.
   1. [ ] Follow up on work laptop. Will need to buy it myself from the 🇲🇽 Dell store and expense it.

#### 2021-08-13

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. Continue working on [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054).
      - [ ] Make sure NPM packages MR is in a good spot for review: tweak MR description and labels, go through the checklist in the MR description template, rebase to a single commit with a good commit message (check the dev docs to learn what we want in the commit message).
      - [ ] Learn how to test the fix manually in local dev environment?
      - [x] Following Steve and David's suggestions, read on issue async updates and added [current status for the issue and first MR](https://gitlab.com/gitlab-org/gitlab/-/issues/330475#note_650843061). **Note:** Unfortunately, didn't have much actual progress on it because I was very tired at the end of the day (and week) after taking care of a personal errand 🚗 🧑‍🔧 that took more than half of my day.
   1. [x] Read the [Package Group](https://about.gitlab.com/handbook/engineering/development/ops/package/) handbook page.
1. Other:
   1. [x] Follow up on work laptop.
      - Having one of the issues Marc was having in Dell's site (won't let me buy the laptop as it detects an invalid configuration). Need to ask him how he got around it and/or contact Dell chat support.

### Week 33 (August 16)

#### 2021-08-16

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. [x] Continue working on [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054). Make sure NPM packages MR is in a good spot for review: tweak MR description and labels, go through the checklist in the MR description template, rebase to a single commit with a good commit message (check the dev docs to learn what we want in the commit message).
      - Not completely done with this, but I went through the MR checklist and read almost all the referenced handbook and dev docs pages that were applicable. **Still pending:**
        - Have one more look at the [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html) (almost sure I've read that page before 😄).
        - Do the couple unit test improvements I identified, and rebase to a single commit with a good (conventional commit?) comment.
        - I still want to review the MR workflow / label workflow docs even though it may not be absolutely necessary for this first MR.
      - While going through the MR checklist I identified a couple things that can be improved in the unit tests 👍.
   1. [x] Continue reading the handbook and development docs to learn more about our development workflows and practices.
      - Read the following pages as I was going through the MR checklist:
        - [Style guides](https://docs.gitlab.com/ee/development/contributing/style_guides.html)
        - [Testing standards and style guidelines](https://docs.gitlab.com/ee/development/testing_guide/index.html)
        - [Testing best practices](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html)
1. [x] Sync with Hayley.
1. [x] Sync with Jõao.

#### 2021-08-17

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. Continue working on [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054).
      - [x] Had a look at the [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html). I think I had read that page before, but I went into more details this time around.
      - [x] Read some of the pages under the [Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/) docs. The code review guidelines let me to those docs. So, I went through the most interesting / relevant topics I found there.
      - No actual code changes. I keep underestimating the amount of info from the handbook, product and dev docs I still need to go through. But I learned a few things that will be useful and want to apply 🙂. For instance:
        - The "Squash commits when merge request is accepted" option. It doesn't work the way I thought it worked, and seems more appealing to me now!
        - Using git push options (e.g. to skip CI pipeline from running), and adding git aliases using git config.

#### 2021-08-18

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. Continue working on [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054).
      - [x] Did the couple unit test improvements I had identified.
      - [x] Asked David about this from the [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html): _"If your merge request includes Product Intelligence (telemetry or analytics) changes, it should be reviewed and approved by a Product Intelligence engineer."_ → David and Steve confirmed this is not necessary and that Danger bot does a good job identifying when it is 👍.
1. Do a second try at buying my work laptop at Dell online store.
   1. [x] I've finally completed the purchase and if everything goes well I should receive an email confirmation with ETA in the next 3 days. The purchase experience from Dell has been bad, and unfortunately there was no way around having to pay for Windows.
1. [x] 1-on-1 with Dan

#### 2021-08-19

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. Continue working on [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054).
      - [x] Read about [Work Type Classification](https://about.gitlab.com/handbook/engineering/metrics/#work-type-classification) labels and use the correct ones for this MR.
        - Added the `bug` label. The rest had already been automatically added.
      - [x] Reviewed the [Merge requests workflow](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html), specially the [Commit message guidelines](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#commit-messages-guidelines).
        - Configured a git commit message template based on [the one suggested in Merge requests workflow]((https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#commit-message-template)).
1. [x] Follow up on work laptop purchase. I was notified of billing issues via email this morning.
   - Had some back and forth with a Dell sales executive. We tried a different payment method that worked after some trial an error. I received a purchase confirmation email, but they (Dell) may still do some payment validation.
1. [x] Weekly team retro.
1. [x] Sync with Steve.
1. [x] Did some quick investigation on how to back up / manage .dotfiles using a git bare repository. Found some good resources on this and added a task in my backlog to address later.

#### 2021-08-20

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. Continue working on [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054).
      - [x] Read the [Engineering Workflow](https://about.gitlab.com/handbook/engineering/workflow/) handbook page.
        - One of those tick pages with a lot of information and links. I have added one agenda topic and one question related to this in my 1-on-1 with Dan this week.
1. [x] Weekly Slack channels catch-up.

### Week 34 (August 23)

#### 2021-08-23

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. Continue working on [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054).
      - [x] Read the [Issues workflow](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html) dev docs page. Most challenging part is getting a good grasp of how we do triaging for different kinds of work, and how to use all the labels. Also, I think I'm understanding a bit better how we organize the product, the work, and the teams into sections, stages, groups, categories, features.
      - [x] Started learning about Snowplow and how we use it for event analytics, using the [Snowplow](https://about.gitlab.com/handbook/business-technology/data-team/platform/snowplow/) handbook page as the starting point.
1. [x] Follow up on work laptop purchase. Possible payment validation was pending.
   - Checked the order status and the order was marked as pending in Dell's site. Told about this to the Dell sales exec and she confirmed that what the Website says is wrong and the order is currently _in production_. She cannot give an exact delivery date, but only the normal estimate of 15-20 days.

#### 2021-08-24

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. Continue working on [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054).
      - [x] Completed my initial learning about [Snowplow](https://about.gitlab.com/handbook/business-technology/data-team/platform/snowplow/).
      - [x] Read the [Snowplow Guide](https://docs.gitlab.com/ee/development/snowplow/#developing-and-testing-snowplow) dev docs page.
        - Went through a lot of information and I think I've gained enough context about product intelligence / data analytics / backend tracking events / Snowplow / Sisense for this issue.

#### 2021-08-25

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. Continue working on [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054).
      - [x] Ping Steve and David in the MR to discuss about how this could be verified in Production.
        - Discussed this with Steve via Slack and have also asked for David's opinion in the MR.
1. [x] 1-on-1 with Dan.
1. [x] Catch up on email notifications and a few issues I should / would like to be following.
   - Spent some time reading the descriptions and commentary on recent PostgreSQL subtransactions issues.
   - Looked at the [14.2 Package retrospective](https://gitlab.com/gl-retrospectives/package/-/issues/26) issue.
   - Looked and added some comments in [Michelle's onboarding journey](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/8) issue 😃.

#### 2021-08-26

1. [x] Weekly retro.
1. [x] Coffee-chat with Michelle.
1. [x] Catch up on Slack channels.
1. [x] Look at last month of activity in [Engineering Week-in-Review](https://docs.google.com/document/d/1GQbnOP_lr9KVMVaBQx19WwKITCmh7H3YlgO-XqVwv0M/edit) doc.
1. [x] Sign up for Culture Amp account and find out what I need to do for the 360 reviews.
1. [ ] Learn to use Expensify and submit Dell laptop expense report to get reimbursed next month.
   - Started looking into this, but haven't submitted my report yet.

### Week 35 (August 30)

#### 2021-08-30

1. [x] Submit Dell laptop expense report in Expensify. [Handbook guideline](https://about.gitlab.com/handbook/spending-company-money/#guidelines) is to report on same month of purchase. So, should do this before Wednesday. Also, according to [Remote's policy](https://about.gitlab.com/handbook/finance/expenses/#global-upside--remotecom), I need to report before 7 of the month to get reimbursed on next pay 🙂.
   - Spent more time than I wanted on this. Process from handbook was a bit confusing, But most importantly, Dell's website and shopping experience is awful. I realized very late that I had been looking at the wrong order #, and I actually had 2 orders (one cancelled, one valid) 🤦‍♂️.
1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5):
   1. Continue working on [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054).
      - [ ] Complete onboarding task about creating and uploading a package to the package registry.
        - Started reading the Package dev docs and the page on NPM packages. Created a project to experiment with this package type locally.

#### 2021-08-31

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5): [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054)
   - [ ] Complete onboarding task about creating and uploading a package to the package registry.
     - I have been playing more on my local GDK-based GitLab instance, but hit a roadblock when trying to create a project using SSH. Spent some time troubleshooting SSH config and searching the docs to find a solution. Learned a bit about how this works, but didn't find a solution yet.

#### 2021-09-01

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5): [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054)
   - [ ] Complete onboarding task about creating and uploading a package to the package registry.
     - After some more troubleshooting, sorting out a new `gdk update` issue, and confirming I didn't know what I was doing trying to change the GDK SSH setup… I was able to set up my SSH config correctly for Git over SSH with the default GDK config (there was no need to tweak anything in GDK).
     - Initialized a dummy NodeJS project to start testing NPM packages.
1. [x] 1-on-1 with Dan.

#### 2021-09-02

1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5): [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054)
   - [x] Complete onboarding task about creating and uploading a package to the package registry. Test NPM packages using dummy NodeJS project.
     - I have published my first package in my local GDK instance 🙌 📦. I'm understanding in more detail how this works 😬.
1. [x] Sync with Steve.

#### 2021-09-03

1. [x] Review [Add dependency proxy image prefix to group type (!69114)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/69114#note_662245966).
1. [x] Complete GitLab Phishing Training.
1. [x] Provide 360 feedback.

### Week 36 (September 6)

#### 2021-09-06

1. [x] Fix laptop battery issue: the battery won't charge either via AC charger or Thunderbolt port.
   - Spent more time than expected troubleshooting and searching for a solution until I found something that worked.
1. [x] Watch recorded [Package Quad Planning meeting](https://youtu.be/Jek766MYWxY).
1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5): [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054)
   - [x] Test bug fix locally using [Snowploy Micro](https://docs.gitlab.com/ee/development/snowplow/index.html#snowplow-micro).
     - Confirmed the fix works correctly using this method (TODO: add this to the MR):
       1. Set up Snowplow Micro locally.
       1. Using `master` branch, try publishing a duplicate NPM package and see it fail with `403 Forbidden`.
       1. Go to <http://localhost:9090/micro/all> and confirm there is 1 good event. Go to <http://localhost:9090/micro/good> and see an event with `se_category: "API::NpmPackages"`, `se_action: "push_package"` was created even when the package push failed.
       1. Go to <http://localhost:9090/micro/reset> to clear the existing event.
       1. Using `330475-fix-npm-push_package-event-created-on-api-error` branch, try publishing a duplicate NPM package and see it fail with `403 Forbidden`.
       1. Go to <http://localhost:9090/micro/all> and confirm there are no events, meaning the fix worked.
       1. Bump the package version and try publishing again, seeing it succeed this time.
       1. Go to <http://localhost:9090/micro/all> and confirm there is 1 good event. Go to <http://localhost:9090/micro/good> and see an event with `se_category: "API::NpmPackages"`, `se_action: "push_package"` was created, meaning success case still works correctly after the fix.
   - [x] Rebase my commits to catch up with `master`, improve the commit messages and get rid of a Danger bot warning.

#### 2021-09-07

1. [x] Watch the [14.2 milestone retrospective](https://youtu.be/Eym11BtJ0_g).
1. [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5): [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) / [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054)
   - [x] Add instructions on "How to setup and validate locally" in the MR description. Referece docs on NPM package creation and [Developing and testing Snowplow](https://docs.gitlab.com/ee/development/snowplow/#developing-and-testing-snowplow).
   - [x] Update MR, and request a review.
     - Received reviewer approval and rebased again to make the branch closer to `master` and fix another Danger bot warning. Also looked into a broken master issue (failing unit test).

#### 2021-09-08

1. [x] Sync with David.
1. [x] Address review feedback on [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054).
1. [x] Watch the [Think BIG recording](https://youtu.be/8G-F7oML-Tk).
   - Provided [feedback about documentation improvements](https://gitlab.com/gitlab-org/gitlab/-/issues/340455#note_672813531) in the the [Make the Package Registry docs easier to follow (#340455)](https://gitlab.com/gitlab-org/gitlab/-/issues/340455) issue. And I've planned to propose an MR to improve the [npm packages in the Package Registry](https://docs.gitlab.com/ee/user/packages/npm_registry/#authenticate-to-the-package-registry) docs page.
1. [x] Attend non-Package meetings related to stock options (or at least look at the agenda notes).
   - Couldn't attend, but looked at the agenda notes and linked materials.

#### 2021-09-09

1. [x] Watch [weekly retro](https://youtu.be/ac0V94R4ep8).
1. [x] Sync with Katie.
1. [x] Sign up and learn to use Carta to accept my new hire options grant.
   - Spent some time reading the thick agreements / linked docs, and doing a bit of additional investigation on stock options and RSUs.

#### 2021-09-10

1. [x] Catch up on engineering Slack channels.
1. [x] Catch up on latest notes in the [Engineering Week-in-Review](https://docs.google.com/document/d/1GQbnOP_lr9KVMVaBQx19WwKITCmh7H3YlgO-XqVwv0M/edit#heading=h.wl5oryd6kv3u).
1. [x] Follow up on my open MR ([gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054)).
   - I've pinged the maintainer to get it merged after I addressed some feedback from David 👍.
1. [x] Check if I have access to Staging for verification of my MRs.
   - I do have access 👍.

### Week 37 (September 13)

#### 2021-09-13

1. [x] Sync with Hayley.
1. [x] Look at today's [Weekly Sync notes](https://docs.google.com/document/d/1p5kOeIAqeZI1sViB6zPsONspN8dsCHeP86_p5yNydnU/edit?usp=sharing).
1. [x] Start working on next MR for [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
   - Started working on the fix for Maven packages. Looked at the Maven packages API code to get familiar with it. Identified where the fix should be applied and started looking at the specs to find where the non-creation of the `push_package` event should be tested.

#### 2021-09-14

1. [x] Sync with Tim.
   - We talked about Package metrics, and Tim gave an overview of the Sisense dashboards he uses the most. The ones containing most of the important information would be the [Package GitLab.com Stage Activity dashboard](https://app.periscopedata.com/app/gitlab/527857/Package-GitLab.com-Stage-Activity-Dashboard), and the [Package: User Adoption and Growth dashboard](https://app.periscopedata.com/app/gitlab/805350/Package:-User-Adoption-and-Growth). He also mentioned he regularly updates the [Ops Section Product Performance Indicators](https://about.gitlab.com/handbook/product/ops-section-performance-indicators/#packagepackage---smau-gmau---count-of-users-publishing-packages) in the handbook with the most important information for business (and there's also some useful analysis / lessons learned there).
1. [x] 1:1 with Michelle.
1. [x] Look at our [Risk Mapping for the Package team (&5673)](https://gitlab.com/groups/gitlab-org/-/epics/5673) epic.
   - This just entered my radar and decided to have a look at the epic. Looking forward to talking about this and the [Package Group Risk Map](https://gitlab-org.gitlab.io/ci-cd/package-stage/risk-mapping/) with someone in a sync meeting.
1. [x] Look at today's Quad Planning meeting [recording](https://youtu.be/77EzeqWBXXU) and [agenda notes](https://docs.google.com/document/d/10toSjnD_qNoR4rOYFjxspTyzXzgps3eIyEigj6OcC2o/edit#).
1. [x] Learn how [GitLab.com auto-deploy releases](https://about.gitlab.com/handbook/engineering/releases/#gitlabcom-releases) work and get familiar with the process for doing manual QA on the different environments.
   - My first Package MR was merged and I got an email notification from the GitLab Release Tool that prompted me to learn about this in order to test my changes in Staging. My MR is included in this [QA issue](https://gitlab.com/gitlab-org/release/tasks/-/issues/2896). It seems the issue is just for tracking purposes and will be closed automatically in ~24 hrs, which should be the time I have to test my changes in Staging. After talking to Steve, if QA is successful in the different environments, the process would be to mention the status of each MR in the [Package issue](https://gitlab.com/gitlab-org/gitlab/-/issues/330475) via async updates. Since it's an issue being fixed with multiple MRs, its status should stay as `workflow::in dev`.

#### 2021-09-15

1. [x] Explore how to test [gitlab-org/gitlab!68054](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054) in Staging. Need to find out if this will be possible, as it seems we don't have the tooling to QA snowplow events in environments different from Production (and in Production I may not have the required access).
   - I pinged the Product Intelligence team in Slack to find if there's a good alternative for this, and validate my understanding of the current state of Snowplow events testing on live environments.
1. [x] Sync with Steve. Talk about Grafana and maybe have him give an overview of our dashboard ([stage-groups: Group dashboard: package (Package)](https://dashboards.gitlab.net/d/stage-groups-package/stage-groups-group-dashboard-package-package?orgId=1)). Also ask about the dashboard available at <https://log.gprd.gitlab.net/> (ELK / Elastic Stack / Kibana?).
   - Steve provided a nice overview of the different dashboards we may use: Grafana, Kibana, and Sentry.
1. [x] Review the [Milestone 14.4 review and discussion (#337966)](https://gitlab.com/gitlab-org/gitlab/-/issues/337966#note_675059030) issue.

#### 2021-09-16

1. Enjoy 🇲🇽 holiday.

#### 2021-09-17

1. [x] Post-holiday Friday catch up / Inbox 0.
1. [x] Look at the Package Weekly Retro ([video](https://youtu.be/tSCOQVrw_mQ) / [notes](https://docs.google.com/document/d/1s1aFQFSq83iUOMdlzPJtckPaGRigw8_aGezau_ASYXs/)) from day before. BTW, I was very busy thinking about my holiday and forgot to post my update 🙄.
1. [x] Follow up on testing of Snowplow events on Staging. I received a response about this from `#g_product_intelligence`.
   - My assessment about what's possible right now is correct. But there are a couple things I can look deeper into:
     1. Snowplow events are also collected from Staging and visible in Sisense using the [`snowplow_staging_unnested_events_30`](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake) or [`snowplow_unnested_events_all`](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.snowplow_unnested_events_all) tables with `where GSC_ENVIRONMENT =  'staging'`. Using Sisense, I wouldn't require access to Snowflake.. I can query the tables using event category and label in combination with GitLab standard context filed to filter events for a given environment:

        ```sql
        select
        *
        from legacy.snowplow_unnested_events_30
        where GSC_ENVIRONMENT = 'staging'
          and se_action = 'push_package'
          and se_category = 'API::NpmPackages'
        ```

        Sisense docs say that to be able to work with queries I'd need the SQL User user type. [The handbook also contains some information about this](https://about.gitlab.com/handbook/business-technology/data-team/platform/periscope/#self-service-dashboard-development):

        >An Editor license is required to develop Sisense reports and dashboards. Editor licenses cost money, so please make sure you need one before requesting one. Keep in mind our frugality value.
     1. There's also another alternative:
        >There is one alternative available, you can also consider using Prometheus counter monitoring successful events sent from each of environments. This counter provides only highly aggregated information about number of successful events, but since staging has very low traffic it is possible to find a moments where no events are send and counter shows flat line. You can query this counter thorough grafana explore tab.
        >[Example query](https://dashboards.gitlab.net/explore?orgId=1&left=%5B%22now-1h%22,%22now%22,%22Global%[…]false%7D,%7B%22ui%22:%5Btrue,true,true,%22none%22%5D%7D%5D) `sum(rate(gitlab_snowplow_successful_events_total{env="gstg"}[5m]))`.
1. [x] Review [Refactor package factories (!70445)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70445).

### Week 38 (September 20)

#### 2021-09-20

1. [x] Look at [Package Weekly Sync notes](https://docs.google.com/document/d/1p5kOeIAqeZI1sViB6zPsONspN8dsCHeP86_p5yNydnU/).
1. [x] Learn a bit more about GitLab ChatOps and try `/chatops` commands in Slack.
   - I discovered that we have another GitLab instance / environment (ops.gitlab.net) that is the one used to authenticate for `/chatops`. Not superclear how everything works, but I'm seeing we have several instances to take care of different things (not everything is on gitlab.com). It seems part of the reason is to have some isolation / decoupling and because of security concerns 🤔.
   - I found that requesting access to ChatOps is part of the onboarding, and a task I hadn't yet covered. So, I set up my ops.gitlab.net account as per the [docs instructions](https://docs.gitlab.com/ee/development/chatops_on_gitlabcom.html#requesting-access) and [requested access in my onboarding issue](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2845#note_682604420). I got the access (thanks Steve), and tested a couple commands successfully on `#chat-ops-test` 👍.
1. [x] Take care of a couple other easy general onboarding tasks now that I'm looking at that issue again 😛:
   - >Set a reminder to add yourself as a code reviewer for GitLab after working here for 3 months. […] [example for backend](https://gitlab.com/gitlab-com/www-gitlab-com/blob/c895d1c7/data/team_members/person/l/luke-duncalfe.yml#L17-18)
     - I've added a Slack reminder. The 3-month mark is very close and I don't want to appear in the reviewer roulette just yet. So, I set the reminder for November. By then I should be more comfortable with my coding pace in Package and more willing to review other code 😅.
   - >Check to see that you have been added to Engineering Google Group `engineering@gitlab.com`. Google Groups and members are reported [here](https://gitlab.com/gitlab-com/security-tools/report-gsuite-group-members/blob/master/engineering.csv).
     - Yes, I'm there 👍.
   - >Find the recurring Executive Vice President of Engineering's Office Hours event located on the GitLab Team Meeting Calendar and copy it over to your personal GitLab calendar.
     - Turns out this is now named CTO Office Hours on the calendar as EVPE title has been changed to CTO (thanks Steve 👍). Planning to attend next week 📆.
1. [x] In Grafana, look at the Snowplow successful events counter in staging. Example [query](https://dashboards.gitlab.net/explore?orgId=1&left=%5B%22now-1h%22,%22now%22,%22Global%22,%7B%22expr%22:%22%20sum(rate(gitlab_snowplow_successful_events_total%7Benv%3D%5C%22gstg%5C%22%7D%5B5m%5D))%22,%22requestId%22:%22Q-f37fab08-9cf6-4e01-a222-61c69f29f801-0A%22,%22format%22:%22time_series%22,%22instant%22:false%7D,%7B%22ui%22:%5Btrue,true,true,%22none%22%5D%7D%5D) - `sum(rate(gitlab_snowplow_successful_events_total{env="gstg"}[5m]))`
   - Tried the suggested query, but can't really make sense of the resulting graph. I'd have to dig deeper into PromQL and the `gitlab_snowplow_successful_events_total` counter and find if there's some other filter I could use to reduce the number of events counted. Overall, this doesn't seem like a very practical approach for what I need.
   - Thinking for next iteration of trying to test Snowplow events (i.e. my next MR) I could ask Tim to create a new Sisense dashboard, copying all the "Push and Pull" charts from the [Package GitLab.com Stage Activity Dashboard](https://app.periscopedata.com/app/gitlab/527857/Package-GitLab.com-Stage-Activity-Dashboard) and editing the queries to target Staging (`where GSC_ENVIRONMENT = 'staging'`).

#### 2021-09-21

1. [x] 1:1 with Michelle.
1. [x] Look at [Package Quad Planning notes](https://docs.google.com/document/d/10toSjnD_qNoR4rOYFjxspTyzXzgps3eIyEigj6OcC2o/edit#) (async-only this week).
   - Looked a bit more into the [Package Group Risk Map](https://gitlab-org.gitlab.io/ci-cd/package-stage/risk-mapping/) as it was mentioned by Sofia. Previously I had learned about it from looking at [Risk Mapping for the Package team (&5673)](https://gitlab.com/groups/gitlab-org/-/epics/5673). Now I learned the risk map is auto-generated 🤖 by the [Risk Mapping tool](https://gitlab.com/gitlab-org/ci-cd/package-stage/risk-mapping) (that will be rewritten in Go) from the [Risk Mapping project issues](https://gitlab.com/gitlab-org/ci-cd/package-stage/risk-mapping/-/issues) (the risks), and it can be manually refreshed by running the [project pipeline](https://gitlab.com/gitlab-org/ci-cd/package-stage/risk-mapping/-/pipelines).
   - Found this is documented under our group page [in the handbook](https://about.gitlab.com/handbook/engineering/development/ops/package/risk-map/) 🙂.
1. [x] Watch the [FY22 Q2 GitLab Assembly video](https://gitlab.edcast.com/insights/fy22-q2) that has been in my Slack reminders list for a while.
1. [x] Continue working on Maven packages MR for [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
   - Run the specs and started getting familiar with the models and spec-related code for Maven packages. Learned more about how we set up our RSpec tests.

#### 2021-09-22

1. [x] Do work laptop setup to move work away from my personal laptop and scratch a couple compliance task items from my general onboarding issue.
   - Unbox, validate hardware config and test out from Windows.
   - Update BIOS and firmware from Windows.
   - Wipe Windows out and install Fedora Workstation 34 with disk encryption.
   - Fix bigger issues related to NVIDIA hybrid graphics (as always 🤷).

#### 2021-09-23

1. OOO 🌴.

#### 2021-09-24

1. OOO 🌴.

### Week 39 (September 27)

#### 2021-09-27

1. OOO: Belated F&F Day 👨‍👩‍👧.

#### 2021-09-28

1. Catch up after time-off.
   - [x] Email
   - [x] Slack
   - [x] Package Weekly Retro ([Notes](https://docs.google.com/document/d/1s1aFQFSq83iUOMdlzPJtckPaGRigw8_aGezau_ASYXs) | [Recording](https://youtu.be/3H-GGFyc3dU))
   - [x] Package Weekly Quad Planning ([Notes](https://docs.google.com/document/d/10toSjnD_qNoR4rOYFjxspTyzXzgps3eIyEigj6OcC2o) | [Recording](https://youtu.be/q5k_bSJGmhg))
   - [x] [Engineering Week in Review](https://docs.google.com/document/d/1GQbnOP_lr9KVMVaBQx19WwKITCmh7H3YlgO-XqVwv0M)
1. [x] Do code review for [Remove unused composer rake task (!71232)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71232).
   - It was a very simple MR to review, but spent some time looking at related issues and MRs to gain some context and learned a bit about Composer in the process. Also found a couple things I'm curious about and want to discuss on my next sync with Steve.
1. [x] Continue tweaking Linux setup on work laptop. Fedora installation seems to be _usable_ right now, but want to make sure I'm not missing anything necessary from the official [NVIDIA](https://rpmfusion.org/Howto/NVIDIA#Optimus) and [NVIDIA Optimus](https://rpmfusion.org/Howto/Optimus) guides. Also need to tweak different GNOME settings for a better experience.
   - Didn't have time to complete the NVIDIA guides, but did some GNOME tweaking and learned about a couple Fedora / Linux things I haven't used yet (using BTRFS and setting up BTRFS snapshots with Timeshift).

#### 2021-09-29

1. [x] Attend CTO Office Hours call (general onboarding task).
1. [x] Review [Remove irrelevant sections of the template (!12)](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/merge_requests/12#note_690726597).
1. [x] Go through [my Package onboarding issue](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5) and complete or scratch a few tasks I've already taken care of.
1. [x] Review [Improve readability of the cleanup tags service (!71293)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71293#note_689223769).
   - Looked at the current code (the service class and main components used by it), and went through related issues and MRs. There was some prior dense work done to improve performance for this 😺.
   - Did a first pass of the MR changes, but was tired and not able to think clearly by the time I got to it. So, decided to continue later.

#### 2021-09-30

1. [x] Package Weekly Retro.
1. [x] Package Social Call.
1. [x] Sync with Steve.
   - We talked about a couple things I was curious about from reviewing [Remove unused composer rake task (!71232)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71232):
     - Some related MRs that removed background workers ([Remove Composer CacheUpdateWorker (!70067)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70067), [Remove Composer CacheCleanupWorker (!70068)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70068)) were closed in order to follow a more involved release process.
     - [One MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/67843/diffs#62e6ce535027f186d44d8e915df377ed558dc73c) I looked at used a feature flag in a way I found confusing, and I discovered a mistake in the specs ([Feature flags in tests reference](https://docs.gitlab.com/ee/development/feature_flags/#feature-flags-in-tests)).
1. [x] 1:1 with Michelle.
1. [x] Unplanned:
   - Continued the discussion on [Remove irrelevant sections of the template (!12)](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/merge_requests/12).
   - Reviewed [Improve package onboarding template skeleton (!13)](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/merge_requests/13).
   - Commented on [Improve the package onboarding (#9)](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/9#note_684145205).
1. [x] Completed review of [Improve readability of the cleanup tags service (!71293)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71293#note_689223769).

#### 2021-10-01

1. [x] Follow up on Package onboarding improvement issue and MRs ([Improve the package onboarding (#9)](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/9#note_684145205) | [Remove irrelevant sections of the template (!12)](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/merge_requests/12) | [Improve package onboarding template skeleton (!13)](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/merge_requests/13)).
1. [x] **Incoming**: Handoff from Steve to follow up on his issues/MRs while he's on PTO.
   - Will take care of reviewing ([Added deploy token authentication support for composer package registry (!60476)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/60476#note_692890577)).
   - Will take care of a quick MR that should be close to merge ([Misleading Dependency proxy image prefix shown in the GitLab UI (#341888)](https://gitlab.com/gitlab-org/gitlab/-/issues/341888) | [Fix dependency proxy image prefix (!71511)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71511)).
   - For [Remove package/ci table cross joins (!70624)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70624) (for issue [Packages changes to support the ci_* database decomposition (#338861)](https://gitlab.com/gitlab-org/gitlab/-/issues/338861#note_692937725)), I got some context from Steve. Not sure I could grab it during review, but will at least keep an eye on it. And if David takes it, I can help review 🙂.
1. [x] Provide [some more feedback](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/9#note_692949839) from my own onboarding experience on [Improve the package onboarding (#9)](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/9#note_684145205).
1. [x] Resume work on Maven packages MR for [gitlab-org/gitlab#330475](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
   - Completed changes and submitted an MR. Like the fix for NPM, it's basically a 2-line fix. The only thing that is more involved is understanding the specs and finding where to update them. The specs for Maven were somewhat different from NPM.
1. [ ] ~~Add an MR to improve Package onboarding issue template (re-organize sections, improve wording, possibly add more details / suggestions).~~ This is already covered by [Improve package onboarding template skeleton (!13)](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/merge_requests/13), and I have been making suggestions there instead.

### Week 40 (October 4)

#### Weekly Focus

1. Continue work on [Prevent creation of push_package event when package publication fails on package manager APIs (#330475)](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
2. Follow up on Steve's work items (PTO covering).
   - Follow up on review of [Added deploy token authentication support for composer package registry (!60476)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/60476#note_692890577).
   - Take care of quick MR that should be close to merge.
     - [Fix dependency proxy image prefix (!71511)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71511)
     - [Misleading Dependency proxy image prefix shown in the GitLab UI (#341888)](https://gitlab.com/gitlab-org/gitlab/-/issues/341888)
   - Keep an eye on more complex MR (has been grabbed by David; I may review).
     - [Remove package/ci table cross joins (!70624)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70624)
     - [Packages changes to support the ci_* database decomposition (#338861)](https://gitlab.com/gitlab-org/gitlab/-/issues/338861#note_692937725)
3. **Unplanned**: Code review of community contributions for Debian package manager.
   - [Add Debian endpoint for distribution key (!71716)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71716/diffs#3c1a6323563eacf5b2fe01888e67e3320c799943)
   - [Allow authentication with headers in the Debin distribution APIs (!71911)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71911#note_697953442)

#### Weekly Goal

- [x] Have MR for Maven package format ready for review: [Prevent creation of push_package event if Maven package file creation fails (!71618)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71618).

**Goal accomplished** 🎯. The MR was merged ✅.

#### Stretch Goals

These are lower priority. I may as well address other incoming tasks that are more urgent instead of these.

1. [ ] Complete _base_ work laptop setup: Attach disk encryption evidence in general onboarding issue and [tick that box once and for all](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2845#note_682818985).
1. [ ] Review growing list of work items where I've been pinged. Scratch from the list or prioritize for later review if it's something more complex or requires a more in-depth review. **Note**: I'm now trying to look at most of the incoming items immediately instead of adding them to the list (will see if that works better 🤞).
   - [ ] [TTL Policy for the Dependency Proxy (#294187)](https://gitlab.com/gitlab-org/gitlab/-/issues/294187)
   - [ ] [Give CI_JOB_TOKEN permission to access internal packages (#333444)](https://gitlab.com/gitlab-org/gitlab/-/issues/333444)
   - [ ] [Discussion: How to sustainably grow the Package group (#334843)](https://gitlab.com/gitlab-org/gitlab/-/issues/334843)
   - [ ] [#2896](https://gitlab.com/gitlab-com/Product/-/issues/2896)
   - [ ] [Release new package managers support incrementally (#22)](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/22#note_672745470)
   - [ ] [Make the Package Registry docs easier to follow (#340455)](https://gitlab.com/gitlab-org/gitlab/-/issues/340455#note_674283304)
   - [ ] [Proposal: Technical documentation for Package features (SSOT/index) (#52)](https://gitlab.com/package-combined-team/team/-/issues/52#note_676230154)
   - [ ] [Packages code improvements (#340812)](https://gitlab.com/gitlab-org/gitlab/-/issues/340812?__cf_chl_jschl_tk__=pmd_aT8amGdcDNVNJHyjlbDeukymq3GoD2PUaNpiWz1cbAw-1631693187-0-gqNtZGzNAjujcnBszQhl#note_677894393)
   - [ ] [Draft: [CI SKIP] POC split npm package type (!70468)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70468)
   - [ ] [Confidential &6754](https://gitlab.com/groups/gitlab-org/-/epics/6754#note_682799161)
   - [ ] [Add Sentry error tracking for package registry APIs (#250572)](https://gitlab.com/gitlab-org/gitlab/-/issues/250572#note_683499353)
   - [ ] [#246782](https://gitlab.com/gitlab-org/gitlab/-/issues/246782#note_684859929)
   - [ ] [October 2021 Ops Engineering Demos (#12397)](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12397)
   - [x] [Cache tags in the GitLab.com container registry to improve the performance of the API (#329637)](https://gitlab.com/gitlab-org/gitlab/-/issues/329637#note_689382529)
   - [x] [Design: Improved UX for bulk deleting packages (#227715)](https://gitlab.com/gitlab-org/gitlab/-/issues/227715#note_694858505)

#### Retrospective

- Accomplished the weekly goal by the middle of the week. It helped that for this MR, there was already progress from previous week. And also I had a couple days (or nights) where I was _into the zone_.
- On Steve's PTO covering, [!71511](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71511) was merged and I took care of verification and updating/closing the related issue, which was a good learning experience 👍.
- For incoming work item pings, I've kept that list from growing bigger by looking at most of them in the first half of my day. But frequently this does take me some non-trivial amount of time that eats into the focus time left for later in the day. So, it's something worth re-evaluating. I either have to not look at those incoming items as close (skimp comments and linked items) or feel comfortable with that list growing bigger but making sure to go through it at least by the end of the week (which can mean spending an entire day on that 😐).
- I did not expect doing community contribution reviews. The biggest challenge has been getting the context. And at first I wasn't sure how much priority I should give this. Since I accomplished my weekly goal early, I switched focus to both this and looking at incoming work item pings (previous point). After getting a feeling of _the tone_ by looking at many community contribution reviews, I decided to focus on just one of them first, but let the reviewer know (it's the same one for all of them) I would take care of the others next wee. David confirmed that's a good approach (I shouldn't feel ashamed of saying I'm at capacity).
- The new format and added sections here are helping me better organize and reflect on the work done throughout the week, albeit this requiring a bit more work itself 😛.
- I had a couple days were I was _in the zone_ this week. However, I ended up doing most of my focused work during the night and messing up my sleep patterns. By Friday I was really tired and had to sleep and rest more during the day. So, that day wasn't very productive. For some of the work it felt worth it, but the Debian contextualization was the kind of work that doesn't have a more immediate / material deliverable and that felt more painful 😅 .

#### 2021-10-04

1. [ ] ~~Coffee-chat with Nico.~~ Cancelled for better rest and focus 😢 (someday we will have that ☕).
1. [x] Complete GitLab Team Member Values Check-in survey ~~and add as a topic to discuss with Michelle on 1:1~~ (it's there already 👍).
1. [x] Prepare for tomorrow's 1:1. It seems 2 batches of questions have accumulated for this week, and I also may add a couple topics to talk about. These topics are requiring some non-trivial amount of handbook reading / exploration time 😓.
   - Answered all questions, did the handbook exploration and added my topics of interest, and also took care of a couple other action items (e.g. look at linked material from agenda).
1. [x] Look at [14.3 Package retrospective (#27)](https://gitlab.com/gl-retrospectives/package/-/issues/27#note_691797051) and add my reactions and comments.
1. [ ] ~~Package weekly sync~~ (cancelled).
1. [x] Package weekly quad planning.

#### 2021-10-05

1. [x] 1:1 with Michelle.
1. [x] **Unplanned** for today, but was **planned** for the week (I gave priority to this because I saw Tim was wrapping up the planning for 14.5 today): Review backend Package Registry issues in [Milestone 14.5 review and discussion (#341294)](https://gitlab.com/gitlab-org/gitlab/-/issues/341294#note_691806095) to find a good P1/Deliverable candidate to grab for 14.5.
   - [Found a couple candidates](https://gitlab.com/gitlab-org/gitlab/-/issues/341294#note_695651543), but there's some risk that would need to be mitigated if I grab one of those issues.
1. [x] Continue working on [Prevent creation of push_package event if Maven package file creation fails (!71618)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71618). **Note**: For faster delivery, I'm [changing the strategy on how I work on these MRs](https://gitlab.com/gitlab-org/gitlab/-/issues/330475?#note_694233864).
   - Tested publication of [Maven packages](https://docs.gitlab.com/ee/user/packages/maven_repository/) locally using the [`gl_pru` utility](https://gitlab.com/10io/gl_pru) (faster than manually setting up a Maven project).
   - Cheated to [cause an error programmatically](https://gitlab.com/gitlab-org/gitlab/-/issues/330475#note_694549291), and validated the fix locally.
   - Tweaked final details in the MR: self-review, local validation instructions, shorter MR title.
   - Requested a backend review.
1. [x] Steve's PTO coverage:
   - [Fix dependency proxy image prefix (!71511)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71511) was merged. After [asking for some help on bug fix verification](https://gitlab.slack.com/archives/CAGEWDLPQ/p1633479796025500), I was able to [verify the fix and close the issue](https://gitlab.com/gitlab-org/gitlab/-/issues/341888#note_696056422).
   - Responded on request for review of [Add Debian endpoint for distribution key (!71716)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71716). Don't feel too comfortable to review it (may take me some time to get the context), but will do my best.

#### 2021-10-06

1. [ ] ~~Coffee-chat with Sofia.~~ Cancelled for better rest and focus.
1. [x] Work on review of [Add Debian endpoint for distribution key (!71716)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71716).
   - Started gathering some context from previous MRs. The changes in this MR are simple, but the previous ones are way more dense.
1. [x] Follow up on any review feedback for [Prevent push_package event if Maven package file creation fails (!71618)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71618).
   - It has been merged now! ✔️
1. [x] Package Weekly Retro.
1. [x] **Unplanned**: Opened [Track failed uploads in the Package Registry (#342573)](https://gitlab.com/gitlab-org/gitlab/-/issues/342573). This was a [suggestion made on the code review of !71618](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71618#note_696458892) ✋🧠 .
1. [x] Grab something from the stretch goals list if I have the time.
   - Looked at [Cache tags in the GitLab.com container registry to improve the performance of the API (#329637)](https://gitlab.com/gitlab-org/gitlab/-/issues/329637#note_689382529). This has been closed as `wontfix` now in favor of a different fix planned for 15.0, but the conversation there is interesting, and helped me gain a bit more context about the Container Registry.
   - Looked at [Design: Improved UX for bulk deleting packages (#227715)](https://gitlab.com/gitlab-org/gitlab/-/issues/227715#note_694858505). Also really interesting and closer to my domain I guess. I had a few questions and comments for the related backend work that would drive the bulk deletion of packages and package files.

#### 2021-10-07

1. [x] Look at pings / follow up on work items I've been _watching_.
1. [ ] Complete review of [Add Debian endpoint for distribution key (!71716)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71716).
   - Haven't completed the review, but I went through (a lot of) issues/MRs, looked at the docs and explored the code locally a bit. And now I've got some understanding of the Debian repository and Debian distributions 👍🏼. I did a first pass of the changes and it's looking good, but I do want to add a couple questions that I left for later because I was really tired after all the exploration.
1. [x] Look at [Allow authentication with headers in the Debin distribution APIs (!71911)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71911#note_697953442), where I've also been pinged for review.
   - Slightly looked at the MR and current comments. I mostly wanted to respond the ping first. I've assigned myself to review although I'd jump to this once I finish with [!71716]((https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71716)) and very probably take a couple days for other work (worst case scenario by the middle of next week). Of course I said this to the contributor, and he was fine with it 👍.

#### 2021-10-08

1. [x] Sync with David.
1. [x] Follow up on watched work items / inbox 0.
   - **Highlight**: Responded to a request to review [Add Helm metadata to GraphQL (!71963)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71963). This would be my focus for the second part of next week. I'm thinking this will be a similar situation to the Debian Repository MRs where I need a bit of time for contextualization and to get started with Helm and GraphQL. This is an MR from the same community contributor BTW 🙂.
1. [ ] Complete review of [Add Debian endpoint for distribution key (!71716)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71716)
   - Started elaborating the review, and I found more things I wanted to comment or ask about. Ended up not submitting the review yet because I wanted to add more details for a couple questions (e.g. wanted to have a quick look at Debian docs to understand something better).

### Week 41 (October 11)

#### Weekly Focus

1. Continue work on [Prevent creation of push_package event when package publication fails on package manager APIs (#330475)](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
2. Follow up on Steve's work items (PTO covering).
   - Follow up on review of [Added deploy token authentication support for composer package registry (!60476)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/60476#note_692890577).
   - May help review more complex MR that has been grabbed by David.
     - [Remove package/ci table cross joins (!70624)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70624)
     - [Packages changes to support the ci_* database decomposition (#338861)](https://gitlab.com/gitlab-org/gitlab/-/issues/338861#note_692937725)
3. Review community contribution MRs for Debian and Helm.
   - [x] [Add Debian endpoint for distribution key (!71716)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71716/diffs#3c1a6323563eacf5b2fe01888e67e3320c799943)
     - Review completed, and MR approved after a couple rounds.
   - [ ] [Allow authentication with headers in the Debin distribution APIs (!71911)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71911#note_697953442)
   - [ ] [Add Helm metadata to GraphQL (!71963)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71963)

#### Weekly Goal

- [x] Have MR for Composer package format ready for review: [Prevent push_package event if Composer package creation fails (!72192)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72192).

**Goal accomplished** 🎯. The MR is ready for review 🕵️.

#### <a id="week-41-stretch-goals"></a>Stretch Goals

These are lower priority. I may as well address other incoming tasks that are more urgent instead of these.

1. [ ] Complete _base_ work laptop setup: Attach disk encryption evidence in general onboarding issue and [tick that box once and for all](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2845#note_682818985).
1. [ ] Review growing list of work items where I've been pinged. Scratch from the list or prioritize for later review if it's something more complex or requires a more in-depth review.
   - [x] [TTL Policy for the Dependency Proxy (#294187)](https://gitlab.com/gitlab-org/gitlab/-/issues/294187)
   - [x] [Give CI_JOB_TOKEN permission to access internal packages (#333444)](https://gitlab.com/gitlab-org/gitlab/-/issues/333444)
   - [x] [Discussion: How to sustainably grow the Package group (#334843)](https://gitlab.com/gitlab-org/gitlab/-/issues/334843)
   - [x] [#2896](https://gitlab.com/gitlab-com/Product/-/issues/2896)
   - [ ] [Release new package managers support incrementally (#22)](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/22#note_672745470)
   - [ ] [Make the Package Registry docs easier to follow (#340455)](https://gitlab.com/gitlab-org/gitlab/-/issues/340455#note_674283304)
   - [ ] [Proposal: Technical documentation for Package features (SSOT/index) (#52)](https://gitlab.com/package-combined-team/team/-/issues/52#note_676230154)
   - [ ] [Packages code improvements (#340812)](https://gitlab.com/gitlab-org/gitlab/-/issues/340812?__cf_chl_jschl_tk__=pmd_aT8amGdcDNVNJHyjlbDeukymq3GoD2PUaNpiWz1cbAw-1631693187-0-gqNtZGzNAjujcnBszQhl#note_677894393)
   - [ ] [Draft: [CI SKIP] POC split npm package type (!70468)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70468)
   - [ ] [Confidential &6754](https://gitlab.com/groups/gitlab-org/-/epics/6754#note_682799161)
   - [x] [Add Sentry error tracking for package registry APIs (#250572)](https://gitlab.com/gitlab-org/gitlab/-/issues/250572#note_683499353)
   - [ ] [#246782](https://gitlab.com/gitlab-org/gitlab/-/issues/246782#note_684859929)
   - [ ] [October 2021 Ops Engineering Demos (#12397)](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12397)

#### Retrospective

- It was great completing my first community MR review. Felt like the completion of last week's effort to dive into something I knew very little about (the Debian Repository). It required extra effort, but should be worth it.
- Accomplished my weekly goal, but it would have been nice to have the MR reviewed and merged. But that didn't happen because: 1) shorter week, 2) having to do local testing caused the MR to be in draft state for at least 1.5 days, 3) because of the PCL probably many people is rushing to complete their tasks and there's less focus on reviewing MRs?
- This week, the learning-related highlight would be gaining some context about the current state of the Composer Repository 🤓, and how it's a bit different from other package formats.
- On Steve's PTO covering, this week I practically didn't need to do anything 😬😌.
- This week there wasn't as much _unplanned work_ as last week, which allowed me to focus more on my weekly goal.
- It felt good (kind of relieving) to decline a code review request I knew I was not going to be able to handle right now.
- Continuing with the trend from last week, my work schedule is still not very _normal_, but this week I found myself taking longer breaks and having more naps 😬.
- From this week's 1:1, and continuing to reflect on my current approach to dealing with pings / email notifications and incoming work, I'd like to experiment with using GitLab To Dos and email filters, and more aggressively scoping / time-boxing non-focus work. I think I'd do this for 14.6. I'd need a bit of time at the start of the milestone to go through and purge my current To Dos 😅.

#### 2021-10-11

1. [ ] ~~Sync with Hayley~~ cancelled for better rest and focus. **Note**: For today I'm doing half-day of work on Sunday and half-day on Monday because of a personal errand that will force me to wrap up early on Monday.
1. [ ] ~~1:1 with Michelle~~ postponed.
1. [x] Finish and submit code review for [Add Debian endpoint for distribution key (!71716)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71716)
   - Submitted. Comments and questions were addressed. Did a second round with one additional suggestion. That was addressed. I approved ✅.
1. [x] Follow up on watched items and pings.
   - **Highlight**: Explored a bit to get more context on [Extract package metadata for npm packages (#330929)](https://gitlab.com/gitlab-org/gitlab/-/issues/330929#note_699916388) as I'll attend a customer meeting on Wednesday related to this issue.
1. [x] Look at Package Wekly Sync ([notes](https://docs.google.com/document/d/1p5kOeIAqeZI1sViB6zPsONspN8dsCHeP86_p5yNydnU) | [video](https://youtu.be/1w1fDmwzkEQ)).
1. [x] Start working on Composer MR for [Prevent creation of push_package event when package publication fails on package manager APIs (#330475)](https://gitlab.com/gitlab-org/gitlab/-/issues/330475).
   - Submitted [Draft: Prevent push_package event if Composer package creation fails (!72192)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72192).

#### 2021-10-12

1. [x] 1:1 with Michelle.
1. [x] Sync with Tim.
   - Discuss focusing on [Properly reply when "go get" gets a user that needs a 2factor authentication (#292732)](https://gitlab.com/gitlab-org/gitlab/-/issues/292732#note_701918186) next week before I leave for PTO. We need to get traction on that issue as early as possible in the milestone (very important to take care of the infradev issues first). It will help that [we've reduced the scope of Prevent creation of push_package event when package publication fails on package manager APIs (#330475)](https://gitlab.com/gitlab-org/gitlab/-/issues/330475#note_701975561), and I'll be able to wrap it up soon this week.
1. [x] Follow up on watched items and pings.
   - **Highlight**: Assessed [planning status of Properly reply when "go get" gets a user that needs a 2factor authentication (#292732)](https://gitlab.com/gitlab-org/gitlab/-/issues/292732#note_701918186). Focusing on that issue because I'd be grabbing it for 14.5. So, it's useful to start getting more context 👍.
1. [x] Look at Package Quad Planning ([notes](https://docs.google.com/document/d/10toSjnD_qNoR4rOYFjxspTyzXzgps3eIyEigj6OcC2o) | [video](https://youtu.be/Q8j1isRRN30)).
1. [ ] Wrap up [Draft: Prevent push_package event if Composer package creation fails (!72192)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72192). Test locally, make sure it's working as expected, and request backend review.
   - I found that [`gl_pru`](https://gitlab.com/10io/gl_pru) doesn't support Composer. So, I started looking at the [Composer Repository docs](https://docs.gitlab.com/ee/user/packages/composer_repository/) to set up a package manually. This led me to do some issues / MRs exploration to learn more about the current state of the Composer Repository and how the implementation it's a bit different from other package formats. So, local testing still pending, but I got some useful background 😄.

#### 2021-10-13

1. [x] Follow up on watched items and pings.
1. [x] Attend customer meeting related to NPM packages metadata.
1. [x] Wrap up [Prevent push_package event if Composer package creation fails (!72192)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72192). Test locally, make sure it's working as expected, update the MR, and request backend review.
1. [x] Think BIG.

#### 2021-10-14

1. [x] Sync with David.
   - We talked about the [`go get` 14.5 infradev deliverable](https://gitlab.com/gitlab-org/gitlab/-/issues/292732), and code reviews of community contributions.
1. [x] Look at Weekly Retro ([notes](https://docs.google.com/document/d/1s1aFQFSq83iUOMdlzPJtckPaGRigw8_aGezau_ASYXs)).
1. [x] Follow up on watched items and pings.
1. [x] Follow up on review of [Allow authentication with headers in the Debin distribution APIs (!71911)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71911#note_697953442).
   - [Removed myself](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71911#note_704403388) as the reviewer for now. David [started discussing](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71911#note_697757350) the motivation / necessity for this MR. So, this is pending agreement and possibly further discussion as a team before moving on.
1. [x] Follow up on review of [Add Helm metadata to GraphQL (!71963)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71963).
   - I've [pinged the community contributor](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71963#note_704257138) to let him know I won't be able to do the review 😓. Because of my focus on [Properly reply when "go get" gets a user that needs a 2factor authentication (#292732)](https://gitlab.com/gitlab-org/gitlab/-/issues/292732) next week, I won't have the time to get the context about Helm charts, GraphQL, and MR specifics. In our priorities, infradev P1 deliverables are the most important tasks and should be handled as early as possible in the milestone (and more so since I'll be leaving for PTO after next week).
1. [x] Start work on [Properly reply when "go get" gets a user that needs a 2factor authentication (#292732)](https://gitlab.com/gitlab-org/gitlab/-/issues/292732).
   - Started reviewing the `Go` middleware code to understand a bit what it does and most importantly, start diving into how it authenticates the user.

#### 2021-10-15

Friends and Family Day… but really want to dedicate some time to stretch goals that I have not been able to address:

1. [ ] Complete _base_ work laptop setup: Attach disk encryption evidence in general onboarding issue and [tick that box once and for all](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2845#note_682818985).
1. [x] Review list of work items (from my [stretch goals](#week-41-stretch-goals) where I've been pinged. Scratch from the list or prioritize for later review if it's something more complex or requires a more in-depth review. Some of them are probably old and no longer relevant.
   - [TTL Policy for the Dependency Proxy (#294187)](https://gitlab.com/gitlab-org/gitlab/-/issues/294187)
     - Looked at this. It has some good context that should be useful for when I have to work with the Dependency Proxy or similar cleanup policies for other product categories. It also helps to get more context about how we do background jobs scalably.
   - [Give CI_JOB_TOKEN permission to access internal packages (#333444)](https://gitlab.com/gitlab-org/gitlab/-/issues/333444)
     - Looked at this briefly. It's been deprioritized, and it's in the backlog right now.
   - [Discussion: How to sustainably grow the Package group (#334843)](https://gitlab.com/gitlab-org/gitlab/-/issues/334843)
     - Had a look at this. I had already gone through most of the discussion.
   - [#2896](https://gitlab.com/gitlab-com/Product/-/issues/2896)
     - Same as above, had already looked at most of the discussion. And a couple months ago I spent some time looking at the company-level discussion about this topic.

### Week 42 (October 18)

#### Weekly Focus

- Infradev P1 deliverable for 14.5: [Properly reply when "go get" gets a user that needs a 2factor authentication (#292732)](https://gitlab.com/gitlab-org/gitlab/-/issues/292732).

#### Weekly Goal

- Have an MR in progress for [Properly reply when "go get" gets a user that needs a 2factor authentication (#292732)](https://gitlab.com/gitlab-org/gitlab/-/issues/292732). Ideally, I'll have an MR ready for review by the end of the week. Worst-case scenario, I'd leave for PTO with most of the work done and hand the in-progress MR off to Steve.

**Goal accomplished** 🎯. The MR is ready for review 🕵️.

#### <a id="week-42-stretch-goals"></a>Stretch Goals

These are lower priority. I may as well address other incoming tasks that are more urgent instead of these.

1. [x] Complete _base_ work laptop setup: Attach disk encryption evidence in general onboarding issue and [tick that box once and for all](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2845#note_682818985).
1. [ ] Review work items where I've been pinged. Scratch from the list or prioritize for later review if it's something more complex or requires a more in-depth review.
   - [x] [Release new package managers support incrementally (#22)](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/22#note_672745470)
   - [x] [Make the Package Registry docs easier to follow (#340455)](https://gitlab.com/gitlab-org/gitlab/-/issues/340455#note_674283304)
   - [x] [Proposal: Technical documentation for Package features (SSOT/index) (#52)](https://gitlab.com/package-combined-team/team/-/issues/52#note_676230154)
   - [ ] [Packages code improvements (#340812)](https://gitlab.com/gitlab-org/gitlab/-/issues/340812?__cf_chl_jschl_tk__=pmd_aT8amGdcDNVNJHyjlbDeukymq3GoD2PUaNpiWz1cbAw-1631693187-0-gqNtZGzNAjujcnBszQhl#note_677894393)
   - [ ] [Draft: [CI SKIP] POC split npm package type (!70468)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70468)
   - [ ] [Confidential &6754](https://gitlab.com/groups/gitlab-org/-/epics/6754#note_682799161)
   - [ ] [#246782](https://gitlab.com/gitlab-org/gitlab/-/issues/246782#note_684859929)
   - [ ] [October 2021 Ops Engineering Demos (#12397)](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12397)

#### Retrospective

- Nice focus this week. I have been able to accomplish my weekly goal by Thursday. **Update**: by Friday afternoon the MR was approved, merged and verified. So, it went even better than expected 😃.
- I keep learning and familiarizing with the codebase. This time I explored a couple topics a bit while working on the `go get` MR:
  - Working with Go packages / modules in GitLab, and the Go Proxy.
  - The Go middleware and its role on the previous point. I don't fully understand this yet because for the sake of velocity I've not tested it end-to-end with a more realistic scenario (e.g. by setting up a Go project and using `go get` manually or using Go module imports that pull from GitLab). By I think I have a rough idea of how that works just by reading the GitLab dev docs and even a bit of the Go docs 😬.
  - Authentication logic used by the Go middleware (which uses some common authentication logic that is also used in other places).
- The non-linear work day trend continues 😬, with more focused work during the evenings/nights, and afternoon naps if tired. This week, at least for a couple days, I also found a bit of time for leisure during the afternoon.
- Since my weekly goal / MR went smoothly, I had some time for stretch goals by the end of the week. It was relieving to take care of the last compliance tasks and having my general onboarding issue be closed :see_no_evil:.

#### 2021-10-18

1. [x] 1:1 with Michelle.
1. [x] Follow up on watched items and pings.
1. [x] Review [14.4 Package retrospective (#28)](https://gitlab.com/gl-retrospectives/package/-/issues/28#note_703252378) and add my feedback.
1. [x] ~~Review [Draft: Add tasks per week and milestones to the package onboarding (!21)](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/merge_requests/21).~~ It has been merged now, but I did have a look at the final changes.
1. [x] Follow up on [Prevent push_package event if Composer package creation fails (!72192)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72192). Ping reviewer or choose another one if there's no response.
   - This was actually reviewed and merged before I woke up 😄.
1. [x] Work on [Properly reply when "go get" gets a user that needs a 2factor authentication (#292732)](https://gitlab.com/gitlab-org/gitlab/-/issues/292732).
   - Gained some additional context, looked at the Go middleware auth code, and reproduced and debugged locally. The fix is straight-forward, and I have a rough idea of how/where to add the specs.
1. [x] Package Weekly Sync (missed the sync meeting :sweat_smile:, but looked at the notes).
1. [x] Package Weekly Quad Planning (async).

#### 2021-10-19

1. [x] Follow up on watched items and pings.
1. [x] Work on [Properly reply when "go get" gets a user that needs a 2factor authentication (#292732)](https://gitlab.com/gitlab-org/gitlab/-/issues/292732).
   - Continued testing locally and created a draft MR with the fix: [Draft: Handle MissingPersonalAccessTokenError on Go middleware (!72671)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72671).
1. [ ] ~~Package Social Call~~. Moved to next week.

#### 2021-10-20

1. [x] Follow up on watched items and pings.
1. [x] Continue Work on [Handle MissingPersonalAccessTokenError on Go middleware (!72671)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72671).
   - Updated the specs, added a changelog trailer, and requested backend and security reviews.
1. [x] Coffee chat with Jaime.
1. [x] Package Weekly Retrospective.

#### 2021-10-21

1. [x] Follow up on watched items and pings.
1. [x] Address any review feedback on [Handle MissingPersonalAccessTokenError on Go middleware (!72671)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72671).
   - Approved by everyone (security, backend, backend maintainer) and merged! ✔️
1. [x] Work on [this week's stretch goals](#week-42-stretch-goals). Really need to resume the work laptop setup 🤐 and send the disk encryption evidence ⚠️.
   - Completed the disk encryption tasks, and with that the general onboarding issue has been closed 🙂.
   - Spent some time tweaking and playing with the `dnf` package manager in Fedora.

#### 2021-10-22

1. [x] Follow up on watched items and pings.
1. [x] Verify [Handle MissingPersonalAccessTokenError on Go middleware (!72671)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72671).
1. [x] Work on stretch goals: continue reducing my list of work items I was pinged on but haven't checked out yet.
   - [x] [Release new package managers support incrementally (#22)](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/22#note_672745470)
     - Added a GitLab to-do to check this later. Hope to check it out when I do my to-do list cleanup.
   - [x] [Make the Package Registry docs easier to follow (#340455)](https://gitlab.com/gitlab-org/gitlab/-/issues/340455#note_674283304)
     - Looked at the latest comments, and found out this'll be worked on on 14.5 👍. Also learned the warnings at the top of each package format docs page was added on a recent MR by Tim. That MR was a very nice improvement I think.
   - [x] [Proposal: Technical documentation for Package features (SSOT/index) (#52)](https://gitlab.com/package-combined-team/team/-/issues/52#note_676230154)
     - Useful initiative. Read all comments and added a couple ideas.

### Week 43 (October 25)

**On Vacation!** 🏖️

#### Stretch Goals

1. [x] Report payment of swag customs tax to get reimbursed (⚠️ must be done before end of the month).
   - Expense report submitted on 10-27.
   - Expense report approved by manager on 10-28.
   - Expense report has been approved by Montpac by 10-30.
1. [ ] Continue work laptop setup.
   - [x] Set up Firefox profiles.
   - [x] Copy Git repos.
   - [x] Install Tilix as terminal emulator (instead of GNOME Terminal).
   - [ ] Tweak Tilix (customize shortcuts, make it minimal, add transparency, etc).
   - [x] Set up Slack.
   - [x] Set up VS Code.
   - [x] Set up Git.
     - Did the custom config from my Linux desktop setup notes. May need further tweaking as per the GitLab handbook instructions.
   - [x] Set up SSH keys.
   - [ ] Set up GDK.
   - [x] Update to Fedora Workstation 35.
     - Updated on 11-02. One highlight is that GNOME is updated to version 41, which supports power modes by default and comes with a nice Power Saver menu in the default _app indicators_ area.
   - [x] Set up Zoom client.

#### Retrospective

I had very good progress with my stretch goals. Nevertheless, the Precision _box_ is not yet ready for development work. So, I'll have to finish the setup in the next couple weeks (depends on how much time I have available for this by end of week after accomplishing my weekly goals). Another option is giving this some priority on week 45, which would then be a catch-up / re-focus / dev environment setup week 🤔.

### Week 44 (November 1)

**On Vacation!** 🏖️

#### Stretch Goals

1. [ ] Inbox zero.
   - Caught up on 11-01. Cleared the inbox and left 12 tabs open for work items I need to catch up on when I get back.
   - Reduced the list of things to see to 3 tabs on 11-02.
1. [ ] Slack zero.
   - Caught up on 10-28.
   - Caught up on 10-30.
1. [ ] GitLab to-do list cleanup (currently ~50 to dos).

#### Retrospective

Did some catching up, but didn't achieve inbox and Slack zero for my second week of vacation due to having to rest for the last 3 days of the week to physically recover 🛏️ (vacation did take its toll).

### Week 45 (November 8)

#### Weekly Focus

- Post-OOO catch up: communications, meetings notes, and status of current milestone.
- Work on something new from current milestone, or grab from next milestone if there's nothing left on the current one.

#### <a id="week-45-goal"></a>Weekly Goal

- Gain context and achieve some progress on [Consider responding with a 401 instead of 404 on unauthenticated request to`GET /api/:version/packages/npm/*package_name` (#334897)](https://gitlab.com/gitlab-org/gitlab/-/issues/334897).

#### <a id="week-45-stretch-goals"></a>Stretch Goals

These are lower priority. I may as well address other incoming tasks that are more urgent instead of these.

1. [x] Finish work laptop setup.
   - [x] Set up GDK.
   - [ ] ~~Tweak Tilix (customize shortcuts, make it minimal, add transparency, etc).~~ This is really optional. So, will do it later. Or will do the minimal tweaking, which shouldn't require much time.
1. [ ] Look at 14.6 milestone planning issue and Rails backend issues where I've been pinged:
   - [ ] [Milestone 14.6 review and discussion (#343044)](https://gitlab.com/gitlab-org/gitlab/-/issues/343044#note_725636076)
   - [ ] [Add the ability to publish and install Conan packages with only name/version (#345055)](https://gitlab.com/gitlab-org/gitlab/-/issues/345055#note_726772811)
   - [ ] ~~[Increase the limit of the listed Helm charts under one repo (#342883)](https://gitlab.com/gitlab-org/gitlab/-/issues/342883#note_726773884)~~ (moved out of 14.6)
   - [ ] [Dependency proxy do not properly validate the user permission for developer role (#283930)](https://gitlab.com/gitlab-org/gitlab/-/issues/283930#note_726782049)
   - [ ] [NuGet package push fails with error: NuGet.Temporary.Package "Invalid Package: failed metadata extraction" (#341655)](https://gitlab.com/gitlab-org/gitlab/-/issues/341655#note_726775786)
   - [ ] [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492#note_729641036)
   - [ ] [Limit the number of build infos pulled in GraphQL (#342881)](https://gitlab.com/gitlab-org/gitlab/-/issues/342881#note_729665007)
   - [ ] [Container Registry GraphqQL: add search and sorting on tag name (#336497)](https://gitlab.com/gitlab-org/gitlab/-/issues/336497#note_730069796)
   - [ ] [Deleting pipeline causes CrossDatabaseModificationAcrossUnsupportedTablesError when nullifying package_build_infos and package_file_build_infos relations (#345664)](https://gitlab.com/gitlab-org/gitlab/-/issues/345664#note_733754033)
1. [ ] Look at older pings. Scratch from the list or prioritize for later if it's something that requires a more in-depth review. Probably most of these no longer require my attention, and I may have seen and disregarded a couple of them recently 😅.
   - [ ] [Packages code improvements (#340812)](https://gitlab.com/gitlab-org/gitlab/-/issues/340812#note_677894393)
   - [ ] [Draft: [CI SKIP] POC split npm package type (!70468)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70468)
   - [ ] [Confidential &6754](https://gitlab.com/groups/gitlab-org/-/epics/6754#note_682799161)
   - [ ] [Confidential #246782](https://gitlab.com/gitlab-org/gitlab/-/issues/246782#note_684859929)
   - [ ] [October 2021 Ops Engineering Demos (#12397)](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12397)

#### Retrospective

- Didn't feel very productive as I did very little milestone work. Only was able to start looking at a new issue by the end of the week 😞. So, re: my weekly goal I was only able to start looking at the issue I grabbed, but haven't started an MR yet.
- Catching up to everything was very difficult. And even though I went through everything, I didn't have enough time to look deeper into the planning of Rails backend issues for next milestone.
- Completing my work laptop setup took a bit more time than expected. And even though my original idea was to timebox that effort to a couple hours, I decided to get it over with once and for all. Now I've transitionted fully to the work laptop and I've scratched that off the list.
- With the year coming to an end, distractions at home / personal errands made it more difficult to focus at times.

##### Lessons

1. Don't be so thorough on catching up after taking more than a few days of time off. Still need to experiment with other workflows that may be more efficient (e.g. only pay attention to GitLab to-dos and Slack pings?). I want to go through my GitLab to-dos to clear that list and start from a blank canvas when I have some time 🙄.
2. Cope with the fact that not all weeks will be as productive?

#### 2021-11-08

1. [x] Package skiplevel meeting.
1. [x] 1:1 with Michelle.
1. Post-OOO catch-up. Timeboxed with no action taking (e.g. go through communications and open browser tabs, but don't look at everything yet… should prioritize and postpone non urgent/important things for later).
   - [ ] Inbox zero.
   - [x] Slack zero.
   - [x] Package weekly sync notes.
   - [x] Package weekly quad planning notes.
   - [ ] ~~Package weekly retro notes.~~ Will catch up on next event.

#### 2021-11-09

1. [x] Package weekly quad planning.
1. [x] Post-OOO catch-up: inbox zero.
1. Look at [current 14.5 milestone](https://gitlab.com/gitlab-org/gitlab/-/issues/341294) and grab something to work on this week.
   - [x] Look at P1 deliverables.
     - All P1 deliverables have been taken care of, including reviews.
   - [x] Look at community contributions.
     - There are no reviews pending. But there's [one MR we may take over](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/60476#note_725624793) and [one we'll definitely take over](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/41446#note_727343677), but seems to require a lot of work that probably should be scheduled for a future milestone. So, no actionable work here yet.
   - [x] Look at stretch goals in current milestone.
     - [#334897](https://gitlab.com/gitlab-org/gitlab/-/issues/334897) was available for grabbing. Steve was considering grabbing it, but turned his focus to other matters and won't have time for it this week. So, grabbing that one! 👍
   - [x] Look at 14.6 milestone planning issue.
     - Got a feeling of the milestone from quad planning, but haven't had a more in-depth look yet. Will leave that for later as the stretch goal from current milestone should be my priority now.
1. [ ] ~~Sync with Tim.~~ I had kind of a late lunch, and took a nap after it. I fell asleep profoundly and missed this call 😨 😢. Had never happened since I normally don't have calls late in the afternoon or late lunchs and naps for that matter 😅.

#### 2021-11-10

1. [x] Look at latest pings.
1. [x] Look at Package Think BIG ([notes](https://docs.google.com/document/d/1G0Q4Cl19R0lx7VQbkZ4HmX7u6OpW4UJzO9pLemThjlE) | [video](https://youtu.be/EkEVpwfZCvQ)).
1. [x] ~~Start work on [Consider responding with a 401 instead of 404 on unauthenticated request to`GET /api/:version/packages/npm/*package_name` (#334897)](https://gitlab.com/gitlab-org/gitlab/-/issues/334897).~~ Complete the dev environment setup on the work laptop.
   - Since this is a stretch issue in the milestone and there should be less time pressure, I decided to use this as an opportunity to set up GDK and have the work laptop transition completed. I haven't started work on the issue, but had some progress with the dev environment setup. However, I wasn't able to dedicate enough time to it because of other distractions.
1. [x] Package weekly retrospective.

#### 2021-11-11

1. [x] Look at latest pings.
1. [x] Complete GDK setup.
   - Had a few issues with GDK and needed to spent some time troubleshooting. But it's ready now!
1. [ ] Start work on [Consider responding with a 401 instead of 404 on unauthenticated request to`GET /api/:version/packages/npm/*package_name` (#334897)](https://gitlab.com/gitlab-org/gitlab/-/issues/334897).

#### 2021-11-12

1. [x] Look at latest pings.
1. [x] Start work on [Consider responding with a 401 instead of 404 on unauthenticated request to`GET /api/:version/packages/npm/*package_name` (#334897)](https://gitlab.com/gitlab-org/gitlab/-/issues/334897).
       - Looked at the issue. I'm a bit confused about the description and discussion. So, will look for clarification.
       - Looked at endpoints code and used this as an opportunity to dig a bit deeper in the codebase and try to find out how the auth is done. Also looked at GitLab dev docs related to API development and the NPM Package Registry.
       - Tried to learn how the official NPM Registry does authentication for private packages. Unfortunately, their API implementation doesn't seem to be open source although they have some docs for their [Public Registry API](https://github.com/npm/registry/blob/master/docs/REGISTRY-API.md). The other way to learn about it is by doing some testing, but the private packages feature is only available in paid plans 😕.
1. [x] Sync with Steve.
1. [ ] If time, look at 14.6 milestone planning issue and Rails backend issues as part of my [stretch goals](#week-45-stretch-goals).

### Week 46 (November 15)

#### Weekly Focus

- [Consider responding with a 401 instead of 404 on unauthenticated request to`GET /api/:version/packages/npm/*package_name` (#334897)](https://gitlab.com/gitlab-org/gitlab/-/issues/334897)
- Virtual GitLab Contribute 2021.
- _Non-work_ stuff with due dates: compliance training and engagement survey.
- [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).

#### <a id="week-46-goal"></a>Weekly Goal

- ~~Have an MR ready for [Consider responding with a 401 instead of 404 on unauthenticated request to`GET /api/:version/packages/npm/*package_name` (#334897)](https://gitlab.com/gitlab-org/gitlab/-/issues/334897).~~ Shifted focus from this. This was blocked and has been moved to the backlog. Only had a bit of progress on clarification and investigation.
- Have some progress on [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).

##### 🎯 Result

Good progress on the 14.6 issue: understanding the security release process, understanding the problem and proposed solutions, and creating the security implementation issue and first MR (empty for now).

#### <a id="week-46-stretch-goals"></a>Stretch Goals

These are lower priority. I may as well address other incoming tasks that are more urgent instead of these.

1. [ ] Look at older pings. Scratch from the list or prioritize for later if it's something that requires a more in-depth review. Probably most of these no longer require my attention, and I may have seen and disregarded a couple of them recently 😅.
   - [ ] [Packages code improvements (#340812)](https://gitlab.com/gitlab-org/gitlab/-/issues/340812#note_677894393)
   - [ ] [Draft: [CI SKIP] POC split npm package type (!70468)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70468)
   - [ ] [Confidential &6754](https://gitlab.com/groups/gitlab-org/-/epics/6754#note_682799161)
   - [ ] [Confidential #246782](https://gitlab.com/gitlab-org/gitlab/-/issues/246782#note_684859929)
   - [ ] [October 2021 Ops Engineering Demos (#12397)](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12397)

#### Retrospective

- 👍 Shorter workweek with a lot of things going on (Virtual Contribute, milestone work, couple _non-work tasks_, couple non-package MRs, fixing a GDK issue) and a shift of focus by mid-week.
- 🎯 Giving that my weekly goal was redefined by mid-week and the nature of the 14.6 issue I grabbed, I think my progress on it was good: understanding the security release process, understanding the problem and proposed solutions, and creating the security implementation issue and first MR (empty for now).
- 👍 I'm glad I was able to do some _extra_ contributions this week, submitting MRs for other projects: one to [fix a GDK issue on Fedora]((https://gitlab.com/gitlab-org/gitlab-development-kit/-/merge_requests/2281)), and one to [fix a typo and improve markdown styles in the security release process developer guide](https://gitlab.com/gitlab-org/release/docs/-/merge_requests/412) 🙂. I also spent a bit of time reading about a recent incident where private package names from the NPM registry were leaked, and learned some security-related stuff in the process (typosquatting, dependency confusion) 👍. It felt good to get something extra out of the week 👌.

#### 2021-11-15

OOO - Holiday (wanted to take care of a couple items from the backlog, but no time 😞).

#### 2021-11-16

1. [x] Virtual GitLab Contribute 2021.
1. [x] Catch-up / pings.
1. [x] Work on [Consider responding with a 401 instead of 404 on unauthenticated request to`GET /api/:version/packages/npm/*package_name` (#334897)](https://gitlab.com/gitlab-org/gitlab/-/issues/334897).
   - Doing some clarification and trying to understand how the auth works for the NPM package metadata endpoints by looking at the specs 🔍.
1. [x] Quad weekly planning.
   - Read the agenda notes. They were fixing my Internet connection issues and I couldn't attend.
1. [x] Take Engagement Survey 2021 - Pulse survey.
1. [x] Complete latest compliance training. The [due date for it was 2021-10-31](https://gitlab.slack.com/archives/C010XFJFTHN/p1632184005321600). But I was OOO, and completely forgot (and didn't see any reminder emails 😞).

#### 2021-11-17

1. [x] Virtual GitLab Contribute 2021.
1. [x] Pings.
1. [x] Clarification on [Consider responding with a 401 instead of 404 on unauthenticated request to`GET /api/:version/packages/npm/*package_name` (#334897)](https://gitlab.com/gitlab-org/gitlab/-/issues/334897).
       - This is blocked and has been moved to the backlog.
1. [x] Weekly retro.
1. [x] Grab P1 deliverable from 14.6: [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).

#### 2021-11-18

1. [x] 1:1 with Michelle.
1. [x] Pings.
1. [x] **Unplanned**: Fix `uuid` library GDK issue 🐛 locally.
   - In preparation to start work on [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492), I dared do `gdk update` and had to troubleshoot and fix an error when GDK tried to update PostgreSQL 😞. Reported this [on Slack](https://gitlab.slack.com/archives/C2Z9A056E/p1637294574367500) and in an [issue where this was fixed for Ubuntu and Debian](https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/1359#note_737739383), hoping to save other people some time!
1. [x] Start work on [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).
   - Since this is my first issue that must go through the security release process, I had a look at:
     - [GitLab release and maintenance policy](https://docs.gitlab.com/ee/policy/maintenance.html)
     - [Security Releases general information](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/process.md)
     - [Security Releases (Critical / Non-critical) as a Developer](https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/security/developer.md)
     - Out of curiosity, also looked at how security issues are reported by external contributors ([GitLab's HackerOne Bug Bounty Program](https://about.gitlab.com/blog/2018/12/12/gitlab-hackerone-bug-bounty-program-is-public-today/)).

#### 2021-11-19

1. [x] Pings.
1. [x] **Unplanned**: Submit [MR with fix for GDK's `uuid` library error on Fedora](https://gitlab.com/gitlab-org/gitlab-development-kit/-/merge_requests/2281).
1. Work on [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).
   - [x] Watch the [security release process from an engineering perspective](https://www.youtube.com/watch?v=ixtUDxM3nWA) video.
   - [x] Read the [releases FAQ](https://about.gitlab.com/handbook/engineering/releases/#frequently-asked-questions).
   - [x] Go through the vulnerability issue to understand the problem and solution.
   - [x] **Unplanned**: Submit [Fix typos and tweak markdown style in security releases developer guide (!412)](https://gitlab.com/gitlab-org/release/docs/-/merge_requests/412).
   - [x] Create the implementation issue.
   - [x] Add the first MR targeting the default branch. It has no commits yet, but wanted to set it up to have that part of the process covered.

### Week 47 (November 22)

#### Weekly Focus

- [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).

#### <a id="week-47-goal"></a>Weekly Goal

- Have MRs for [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).

#### <a id="week-47-stretch-goals"></a>Stretch Goals

These are lower priority. I may as well address other incoming tasks that are more urgent instead of these.

1. [ ] Look at older pings. Scratch from the list or prioritize for later if it's something that requires a more in-depth review. Probably most of these no longer require my attention, and I may have seen and disregarded a couple of them recently 😅.
   - [ ] [Packages code improvements (#340812)](https://gitlab.com/gitlab-org/gitlab/-/issues/340812#note_677894393)
   - [ ] [Draft: [CI SKIP] POC split npm package type (!70468)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70468)
   - [ ] [Confidential &6754](https://gitlab.com/groups/gitlab-org/-/epics/6754#note_682799161)
   - [ ] [Confidential #246782](https://gitlab.com/gitlab-org/gitlab/-/issues/246782#note_684859929)
   - [ ] [October 2021 Ops Engineering Demos (#12397)](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12397)

#### Retrospective

This was a good week:

- 🎯 I think I achieved my weekly goal: "have MRs for [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492)". Originally, this meant the default branch MR ready for review, and the 3 backport MRs. However, the recommendation is to create the backports once the default branch MR is approved by AppSec. So, no backport MRs yet 👍.
- 👍 There was good progress with the implementation for this issue. One hurdle was that I needed some more discussion re: the solution to go for. I investigated, tested, and implemented one solution first. But then I found it had one non-trivial drawback (additional memory cost for failed validations). So, I drafted a different solution that doesn't have that drawback and has the best accuracy with not much added complexity 👍.
- 👍 This week I did another small non-Package contribution: [Fix typo on Engineering Metrics handbook page (!94350)](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/94350).
- 📝 I kept pings at bay and learned a few things:
  - How package files are uploaded for the NPM Registry, and a bit about how uploads work deeper in our Rails stack.
  - I should use the appropriate non-Package group label in non-Package MRs to avoid noise for Product and Engineering Managers. If there's not an appropriate non-Package label, I can use `group::not_owned` (this is a caveat of the GitLab bot as it seems it can't be prevented from putting a group label).

#### 2021-11-22

1. [x] Pings.
   - **Highlight**: I've been assigned a December on-call shift (on 12-31 😮). So, I had to [find out more about it](https://about.gitlab.com/handbook/engineering/development/processes/Infra-Dev-Escalation/process.html) 🤔.
1. [x] Sync with David.
1. [x] Weekly sync.
1. [x] 1:1 with Michelle.
1. [x] **Unplanned**: Submitted [Fix typo on Engineering Metrics handbook page (!94350)](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/94350). This was related to some interesting input I got from the team about which group label to use for MRs targeting non-Package projects.
   - **Update**: It has been merged 👍.
1. [x] Review and add feedback on [14.5 milestone retro issue](https://gitlab.com/gl-retrospectives/package/-/issues/29).

#### 2021-11-23

1. [x] Pings.
1. [x] Weekly quad planning ([notes](https://docs.google.com/document/d/10toSjnD_qNoR4rOYFjxspTyzXzgps3eIyEigj6OcC2o) | [video](https://youtu.be/A_sSJgCgqBQ)).
1. [x] Sync with Hayley.
1. [x] Work on [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).
   - Did some investigation and testing of the proposed solutions.
   - Looked a bit into the [`mitmproxy`](https://mitmproxy.org/) CLI tool for testing/validation.
   - Did some issues, MRs, and docs exploration to find the affected versions for the implementation issue.
   - Learned how to use the feature via CLI and Admin UI.
   - Did some testing and debugging, and found what I think is the best solution.
1. [x] **Follow up**: [Fix "library 'uuid' is required" error for Fedora (!2281)](https://gitlab.com/gitlab-org/gitlab-development-kit/-/merge_requests/2281) was merged 👍.

#### 2021-11-24

1. [x] Pings.
1. [x] Sync with Steve.
1. [x] Weekly retro.
1. [x] Work on [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).
   - Not much progress, but added the results of my investigation and testing in the issue.
1. [x] **Follow up**: [Fix typos and tweak markdown style in security releases developer guide (!412)](https://gitlab.com/gitlab-org/release/docs/-/merge_requests/412) was merged 👍.

#### 2021-11-25

1. [x] Pings.
1. [x] Submit ~~a draft~~ an MR for the default branch for [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).
   - Pushed a fix and requested reviews.

#### 2021-11-26

1. [x] Pings.
1. [x] Work on [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).
   - Followed up on reviews.
   - Discussed the 3 different solutions a bit more. And it became apparent the one I chose is not the best one because of additional memory cost.
   - Pushed a draft MR with a different solution that I think should be the one to go for.

### Week 48 (November 29)

#### Weekly Focus

- [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).

#### <a id="week-48-goal"></a>Weekly Goal

- Have all MRs for [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492) ready. **Note**: Remember to label the issue with `workflow: Awaiting security release`.

#### <a id="week-48-stretch-goals"></a>Stretch Goals

These are lower priority. I may as well address other incoming tasks that are more urgent instead of these.

1. [x] Look at older pings. Scratch from the list or prioritize for later if it's something that requires a more in-depth review. Probably most of these no longer require my attention, and I may have seen and disregarded a couple of them recently 😅.
   - [x] [Packages code improvements (#340812)](https://gitlab.com/gitlab-org/gitlab/-/issues/340812#note_677894393)
   - [x] [Draft: [CI SKIP] POC split npm package type (!70468)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70468)
   - [x] [Confidential &6754](https://gitlab.com/groups/gitlab-org/-/epics/6754#note_682799161)
   - [ ] ~~[Confidential #246782](https://gitlab.com/gitlab-org/gitlab/-/issues/246782#note_684859929)~~ No longer relevant.
   - [ ] ~~[October 2021 Ops Engineering Demos (#12397)](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12397)~~ No longer relevant.

#### Retrospective

- 👎 Security issue didn't make it into this week's release.
- 🎯 ✅ But my weekly goal of having the MRs for [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492) ready was accomplished. There's just a bit of process that needs to be completed once the 14.6 stable branch is available and the next security release is prepared.
- 👍 Reviewing [Remove dependency_proxy_manifest_workhorse ff (!74669)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/74669) made me learn a bit more about feature flag rollouts.
- 👍 I was able to work on this week's stretch goal, and have now cleared the _older pings_ backlog 💥.
- 👍 I was able to start looking at the training issues that are due this month, and have finished the [GitLab reliability training](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/33).
- 👍 For the most part I didn't feel stressed out this week. I think it was a smooth week, and I had enough time to relax.

##### Learnings

- Should try not to stress out so much 🙂. Calm should be the rule and not the exception.

#### 2021-11-29

1. Family and Friends Day.
1. [x] Review [Remove dependency_proxy_manifest_workhorse ff (!74669)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/74669).

#### 2021-11-30

1. [x] Pings / catch up.
1. [x] Work on [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).
   - Addressed feedback on the new MR that has been chosen as the solution to go for.
   - Requested security review.
   - Unfortunately, this won't make it to the security release this week. But I'll try to have all the MRs ready soon and target December's security release.
1. [x] Weekly quad planning.
1. [x] Work on stretch goal (looking at older pings):
   - [Packages code improvements (#340812)](https://gitlab.com/gitlab-org/gitlab/-/issues/340812#note_677894393)
     - This was promoted to an epic: [Packages code improvements (&6753)](https://gitlab.com/groups/gitlab-org/-/epics/6753). I've looked at the epic and a couple issues there and added a couple comments.
   - [Draft: [CI SKIP] POC split npm package type (!70468)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/70468)
     - Looked at this while [enquiring in the Packages code improvements (&6753) epic](https://gitlab.com/groups/gitlab-org/-/epics/6753#note_748788674). Added a comment with an approach that could be worth exploring.

#### 2021-12-01

1. [x] Pings.
1. [x] 1:1 with Michelle.
1. [x] Work on [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).
   - Approvals from backend and security received 👍. Requested backend maintainer review.
1. [x] Weekly retro.
1. [x] Work on stretch goal (looking at older pings):
   - [Confidential &6754](https://gitlab.com/groups/gitlab-org/-/epics/6754#note_682799161). Looked at the issues in that epic, and it's now on my radar.

#### 2021-12-02

1. [x] Pings.
1. [x] Work on the backport MRs for [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492) as it has now been maintainer-approved 🙌.
   - I have created the backport MRs and requested maintainer review. The one for 14.6 needs to wait until `14-6-stable-ee` is created so that I can change the target branch and then request the review.
1. [x] Start [GitLab reliability training](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/33).
   - Looked at 21 cards out of 34.

#### 2021-12-03

1. [x] Pings.
1. [x] Sync with David.
   - **Highlight**: Asked about the NPM request forwarding feature. The main use case is for users that can't use scoped packages because of some limitation. So, they configure the GitLab Package Registry at the global level, and with the request forwarding feature they can still install from the NPM registry.
1. [x] Follow up on [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492). Wait for approval of backport MRs (except the one targeting current branch as the stable branch for it doesn't exist yet), and label the vulnerability issue with `workflow: Awaiting security release`.
   - Backport MRs for 14.4, 14.5 have been approved. 14.6 will have to wait until I can target its stable branch that is yet to be created. I have labeled everything with `workflow: Awaiting security release`: the vulnerability issue, the security issue, the default branch MR, and the backport MRs for 14.4, 14.5, 14.6.
1. [x] Complete [GitLab reliability training](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/33).

### Week 49 (December 6)

#### Weekly Focus

- Help with pending non-P1 milestone work since all P1s have already been grabbed.
- Help with code reviews for milestone MRs if needed.

#### <a id="week-49-goal"></a>Weekly Goal

- Have a draft MR with some progress to take [Exhaustively test authentication mechanisms for package manager APIs (!50729)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/50729) community MR across the finish line 🏁.

#### <a id="week-49-stretch-goals"></a>Stretch Goals

These are lower priority. I may as well address other incoming tasks that are more urgent instead of these.

1. [ ] Look closer at rest of ruby backend issues in 14.6 milestone just to have some context about other work. This time I didn't have enough time to do this before the milestone started 😞.
1. [ ] Look at GitLab To-dos, and reduce the list to 30 items 😨 (currently at 50).

#### Retrospective

- 🎯 I've created the draft MR and tracking issue for [Exhaustively test authentication mechanisms for package manager APIs (#347410) · Issues · GitLab.org / GitLab · GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/347410). However, I didn't have additional progress on it because I switched focus to reviews and then to a Production incident and looking at corrective action issues.
- 👍 I'm happy I was able to help review a couple 14.6 MRs and learned so much in the process!
- 📝 Learned the process for grabbing a community contribution MR to take it across the finish line 🏁.
- 📝 Learned how our `ApplicationRecord.safe_find_or_create_by` and `ApplicationRecord.safe_find_or_create_by!` methods work and a couple gotchas / caveats when using them 😯.
- 📝 Learned how deploy token support will now work for the Composer Registry, and that the Composer CLI provides some [support for GitLab and other vendors](https://getcomposer.org/doc/06-config.md#gitlab-token) via config 🤔.
- 📝 Learned how the legacy API authentication works using Grape route settings and an auth finder. And how deploy access tokens are setup, and how the models work and apparently are prepared to be used for multiple groups and projects in the future (there are cross-reference models/tables) 🤔.
- 📝 Learned a bit about how production incidents, RCAs, and FCLs are handled 🔴.

#### 2021-12-06

1. [x] Pings.
1. [x] Sync with Hayley.
1. [x] Weekly sync.
1. [x] Look at state of Rails / backend issues in the milestone and grab something new. Probably help with community MRs.
   - I grabbed [Exhaustively test authentication mechanisms for package manager APIs (!50729)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/50729) community MR, and started gaining context by looking at it and related issues, MRs, and docs. I pulled and rebased the external branch, but I've found a couple issues pushing it to `gitlab-org/gitlab` as is.

#### 2021-12-07

1. [x] Pings.
1. [x] Weekly quad planning ([notes](https://docs.google.com/document/d/10toSjnD_qNoR4rOYFjxspTyzXzgps3eIyEigj6OcC2o) | [video](https://youtu.be/HAi23SDe3jE)).
1. [x] Work on [Exhaustively test authentication mechanisms for package manager APIs (!50729)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/50729).
   - Pushed an [initial draft MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76324) created from the community MR. And added a [parent issue](https://gitlab.com/gitlab-org/gitlab/-/issues/347410) to track progress.
1. [x] Sync with Tim.

#### 2021-12-08

1. [x] Pings.
1. [x] Review [Fix registry race condition (!75483)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/75483).
   - Took a deep dive into this. I did some investigation , analysis, testing, and even created a [throwaway MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76420) with my findings and a possible alternative solution.
1. [ ] Continue working on [Exhaustively test authentication mechanisms for package manager APIs (#347410)](https://gitlab.com/gitlab-org/gitlab/-/issues/347410).
   - Wasn't able to work on this today as the review of #347410 took more time than expected 😬.
1. [x] Weekly retro.
1. [x] Think big.

#### 2021-12-09

1. [x] Pings.
1. [x] 1:1 with Michelle.
1. [x] Sync with Steve.
1. [x] Follow up on review of [Fix registry race condition (!75483)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/75483).
   - I have approved ✅.
1. [x] **Unplanned**: Look at communications / information from the [Package-related incident](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/6042) that happened today.
1. [x] Review [Deploy token support for the Composer package registry (!75798)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/75798).
   - Approved backend changes ✅. Only left a couple documentation-related comments. Needed some time to get the context, and also used this as an opportunity to see a bit more of the authentication code.
1. [ ] Continue working on [Exhaustively test authentication mechanisms for package manager APIs (#347410)](https://gitlab.com/gitlab-org/gitlab/-/issues/347410).
   - Again, didn't have time to resume work on this 😕.

#### 2021-12-10

1. [x] Pings.
1. [x] Look at corrective action issues which have been made `~severity::1` `~infradev`. Unfortunately, this will mean leaving [#347410 (community contribution work)](https://gitlab.com/gitlab-org/gitlab/-/issues/347410) on pause.
1. [x] Start [Gitlab Psychological Safety Training](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/35), and work on other weekly stretch goals if time allows.

### Week 50 (December 13)

#### Weekly Focus

- Corrective actions for Production incident.
- 14.7 milestone planning / review.
- Career development conversation.

#### <a id="week-50-goal"></a>Weekly Goal

- Work on corrective actions for Production incident (more specific goal TBA). **Update**: It wasn't possible to have a clear goal at the beginning of the week. Around midweek we decided I could start helping with the corrective actions effort by working on MR 2 for [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755).

#### <a id="week-50-stretch-goals"></a>Stretch Goals

These are lower priority. I may as well address other incoming tasks that are more urgent instead of these.

1. [ ] Look closer at ruby backend issues in 14.6 milestone that I didn't have a chance to review before.
1. [ ] Look at GitLab To-dos, and reduce the list to 30 items 😨 (currently at around 50).

#### Retrospective

- 🎯 I didn't have a clear goal starting the week. Around midweek, I dived deeper to understand all the related issues for package files cleanup and the corrective actions for the Production incident and what was needed for MR 2 for [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755). And by the end of the week I had started working on that MR.
- 👍 I followed up on a few corrective actions in the confidential issue for the production incident as I was assigned as the DRI.
- 👍 I did a good amount of reviewing: [1 Package MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77043) useful for my weekly goal, and [1 non-Package MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/95671).
- 👍 I was able to submit and have merged a [non-Package MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/95671).
- 📝 I gained a better understanding on Package Registry cleanup policies, package files cleanup, corrective actions for the production incident, and how the different issues for these are tied together.
- 📝 I started learning about how we use Sidekiq jobs.
- 📝 I learned a bit about how we use authorization policies.
- 📝 I learned more about career development at GitLab, and the different tools that can be used to manage it.
- 👎 I underestimated the amount of work needed to prepare for the career development conversation. After filling the template doc, I felt I could have provided more details and also wasn't clear about a few things in the template. I also found there's not a clear process in the handbook; it provides a lot of suggestions, but it seems different teams may do this in a different way or even different people inside the same team. This is not something bad, but maybe we need to add clarity and improve efficiency by documenting the specifics for us on our [group page](https://about.gitlab.com/handbook/engineering/development/ops/package/) 🤔.

#### 2021-12-13

1. [x] Pings.
1. [x] 1:1 with Michelle.
1. [x] Weekly sync.
1. [x] Review 14.7 milestone.
1. [x] Follow up on Production incident corrective actions, and start working on one of the created issues.
   - Reviewed changes in the corrective action issues and updated info on the incident confidential info issue (links to issues and ETAs).
   - Grabbed [Confidential #348166](https://gitlab.com/gitlab-org/gitlab/-/issues/348166). It's currently blocked, but will follow and review the blocker issue to be prepared to work on it as soon as it's unblocked.

#### 2021-12-14

1. [x] Pings.
1. [ ] Career development conversation preparation.
   - [x] Read the [Career Development](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development) handbook page and look at the [Individual Growth Plan (IGP)](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#individual-growth-plan) linked doc. 👈 This was also an old action item in Lead Honestly.
   - [ ] Fill the [Senior Backend Engineer career development template](https://docs.google.com/spreadsheets/d/1jIiqOClwVn68ucpygYnZvwM8f_wp6BAUsYZG4rhvmM8).
     - Use the "2020-12" tab. Rename it to "2021-12". The "template" tab can be removed.
     - Fill only the "Self assessment", "Notes/examples",
     - Use "Developing - Performing - Exceeding" as described in [the handbook](https://about.gitlab.com/handbook/people-group/talent-assessment/#what-is-performance).
     - Also use the following advice:

       >- Read the competency behaviors for each of the values according to your level (7), example https://about.gitlab.com/handbook/values/#collaboration-competency, this will help you to understand difference between levels and behaviors expected.
       >- Also remember that the scope of senior is not just about technical scope, but impact and influence in the group

   - [ ] Optionally, fill this other [Talent self-assessment template](https://docs.google.com/document/d/1z64Se4iDZdEcQe8hprNqAKfpJoTquy4ts4zZoF_DW8I).
1. [ ] ~~If time , pick up [Unable to delete container tags that have "Invalid tag: missing manifest digest" (#341939)](https://gitlab.com/gitlab-org/gitlab/-/issues/341939) while I wait for corrective actions work? 🤔~~ I don't think I'll have enough waiting time for it 👍.
1. [x] Weekly quad planning ([notes](https://docs.google.com/document/d/10toSjnD_qNoR4rOYFjxspTyzXzgps3eIyEigj6OcC2o/edit#heading=h.xcu6s8i7nata) | [recording](https://youtu.be/kA6nUiLnzWc)).
1. [x] Corrective actions follow-up. Added missing labels in corrective action issues.
1. [x] **Unplanned**: Review [Update product development timeline (!95671)](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/95671).
1. [x] **Unplanned**: Open [Fix typos and wording in Career Development page (!95682)](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/95682), which has now been merged 👍.

#### 2021-12-15

1. [x] Pings.
1. [x] Package weekly retro.
1. [x] Look at current work in [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755) to start working on MR 2 for that issue (issue will be [handed over from David](https://gitlab.com/gitlab-org/gitlab/-/issues/345755#note_777760201) once MR 1 is merged).
   - Reviewed all the related issues (and epic) to better understand how everything will be tied together. I hadn't understood this that well before 🤔:
     - [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755) is the first issue for the [Package Registry cleanup policies (&5152)](https://gitlab.com/groups/gitlab-org/-/epics/5152) effort that wil be spanning multiple milestones. This first issue adds a new more scalable way to delete package files, which will make it possible to use cleanup policies in a reliable way. Also, it doesn't add support for the package level yet.
     - [&5152](https://gitlab.com/groups/gitlab-org/-/epics/5152) epic contains other issues to take care of backend and frontend changes for the clean-up policies themselves (i.e. a new model and CRUD functionality).
     - [Confidential #348166](https://gitlab.com/gitlab-org/gitlab/-/issues/348166) will build upon [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755) to add support for the package level and contain other changes to function as a corrective action for [Confidential #6044](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/6044).
   - Looked at current changes in [Draft: Add package file status attribute and introduce the installable scope (!76767)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767), which is the first MR for [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755). This is needed by MR 2 (the background jobs) in that issue, which I'll be working on. However, I can start work on parallel to MR 1 by adding a dummy way to select package files for now (the real way added by MR 1 is by using a scope that only retrieves package files marked as `pending_destruction`).

#### 2021-12-16

1. [x] Pings.
1. [x] Start working on MR 2 for [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755).
       - Looked at services and workers code. Also looked a bit at authorization policies code out of curiosity. Looked at a couple related pages in the GitLab docs, but would like to dive deeper in the [Sidekiq Style Guide](https://docs.gitlab.com/ee/development/sidekiq_style_guide.html) later.
1. [ ] Complete career development conversation preparation.
   - [x] Fill the [Senior Backend Engineer career development template](https://docs.google.com/spreadsheets/d/1jIiqOClwVn68ucpygYnZvwM8f_wp6BAUsYZG4rhvmM8).
     - Use the "2020-12" tab. Rename it to "2021-12". The "template" tab can be removed.
     - Fill only the "Self assessment", "Notes/examples",
     - Use "Developing - Performing - Exceeding" as described in [the handbook](https://about.gitlab.com/handbook/people-group/talent-assessment/#what-is-performance).
     - Also use the following advice:

       >- Read the competency behaviors for each of the values according to your level (7), example https://about.gitlab.com/handbook/values/#collaboration-competency, this will help you to understand difference between levels and behaviors expected.
       >- Also remember that the scope of senior is not just about technical scope, but impact and influence in the group

     - **Note**: [Full list of values competencies links](https://about.gitlab.com/handbook/competencies/#values-competencies).
     - **Note**: I have filled most of the template, but I'm missing the value competency assessments. Didn't know I'd have to do those. In general, I underestimated the amount of work for this process. Just reviewing/revisiting the handbook material can take quite some time.
   - [ ] Optionally, fill this other [Talent self-assessment template](https://docs.google.com/document/d/1z64Se4iDZdEcQe8hprNqAKfpJoTquy4ts4zZoF_DW8I).

#### 2021-12-17

1. [x] Pings.
1. [x] Sync with David.
1. [x] Review [Call dependency proxy cleanup workers in purge (!77043)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77043).
   - I took [a deep dive into testing Sidekiq workers](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77043#note_784487853). This should help me for my own work on [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755)!
1. [x] Do all value assessments to add the scores in the career development doc.
1. [x] Continue working on MR 2 for [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755).
   - Not much progress, but the review of !77043 has helped me to look closer at the Dependency Proxy workers to use them as an example to implement the new package file jobs.

### Week 51 (December 20)

#### Weekly Focus

- Career development conversation.
- MR 2 (background jobs) for [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755).

#### <a id="week-51-goal"></a>Weekly Goal

- Have MR 2 (background jobs) for [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755) ready for review.

#### <a id="week-51-stretch-goals"></a>Stretch Goals

These are lower priority. I may as well address other incoming tasks that are more urgent instead of these.

1. [ ] Look at GitLab To-dos, and reduce the list to 30 items 😨 (currently at around 50).

#### Retrospective

- 🎯 I wasn't able to have [Draft: Add package file cleanup jobs (!77212)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212) ready for review: in part because it's dependent on MR 1 which was still on review by the end of the week, and in part because the holidays are distracting 😬. However, I had some good progress given the circumstances.
- 👍 I followed up on [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492) and made it ready for the next security release.
- 👍 I did a good amount of reviewing: [1 Package MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767) useful for my work on [!77212](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212), and [1 non-Package MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77224#note_792415419).
- 📝 I learned more about cron jobs and limited capacity jobs while working on [!77212](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212).

#### 2021-12-20

1. [x] Pings.
1. [ ] ~~Sync with Hayley.~~ I cancelled this week,
1. [x] Weekly sync.
1. [x] Career development conversation.
1. [x] Follow up on [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492). Find if stable branch for 14.6 has been cut and if new security release issue has been created or will be created soon.
   - Took care of final preparations now that `14-6-stable-ee` branch and the next security release issue (14.6.x, 14.5.x, 14.4.x) are available.
1. [x] Work on MR 2 for [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755).
   - Created a rough first version of the jobs based on the Dependency Proxy cleanup jobs.

#### 2021-12-21

1. [x] Pings.
1. [x] Weekly quad planning.
1. [x] Work on MR 2 for [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755). Add specs and submit a draft MR.
   - Submitted a draft MR with current progress.
   - Tweaked the cron job and added its specs.
   - Fixed the cleanup job and added its specs.
   - Added spec for new `PackageFile` scope. In hindsight, I think this wasn't needed because it's just a dummy scope, and the real implementation is in MR 1 of this issue 😅 🤦‍♂️ .
1. [ ] ~~Sync with Tim.~~ This will be async this time.
1. [ ] Review [Add package file status attribute and introduce the installable scope (!76767)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767).
1. [x] **Unplanned**: Reviewed [Explain queue routing behaviour in GDK (!77224)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77224#note_792415419).

#### 2021-12-22

1. [x] Pings.
1. [x] Follow up on [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492).
   - The 14.6 backport MR has been approved, and all MRs associated to the security implementation issue have been assigned to `@gitlab-release-tools-bot`.
1. [x] Weekly retro.
1. [x] Follow up on review of [Explain queue routing behaviour in GDK (!77224)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77224#note_792415419).
   - Approved after a couple changes I requested were applied. Then procured a documentation review, and it was merged after a few more tweaks from the documentation reviewer.
1. [x] Review [Add package file status attribute and introduce the installable scope (!76767)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767).
   - It took me a while to review (many changes). Added a few comments and questions for very minor things only.
1. [ ] Work on [Draft: Add package file cleanup jobs (!77212)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212). Figure out a few TODOs to make it ready for review.

#### 2021-12-23

1. [x] Pings.
1. [x] Review follow-up for [Add package file status attribute and introduce the installable scope (!76767)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767).
   - Approved and had a few more comments/questions, only for my own understanding.
1. [x] Work on [Draft: Add package file cleanup jobs (!77212)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212). Figure out a few TODOs to make it ready for review.
   - Fixed a conflict with latest `master`.
   - Investigated and fixed Sidekiq spec failures.
   - Added application setting to make cleanup worker max running jobs configurable.
1. [x] Sync with Steve.

#### 2021-12-24

1. [x] Pings.
1. [ ] Work on [Draft: Add package file cleanup jobs (!77212)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212). Address a couple pending TODOs to have it ready for pre-review. **Note**: MR 1 needs to be merged first so that I can rebase, fix conflicts and have it ready for review.

### Week 52 (December 27)

#### Weekly Focus

- Mostly OOO this week, but want to follow up on [!77212](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212).
- Family & Friends Day (Mon).
- Vacation (Tue-Fri).
- On call shift (Friday 9:00-13:00 MST).

#### <a id="week-52-goal"></a>Weekly Goal

- Get some rest 🌴.

#### <a id="week-52-stretch-goals"></a>Stretch Goals

These are lower priority. I may as well address other incoming tasks that are more urgent instead of these.

1. [ ] Review and comment on [14.6 Package retrospective (#30)](https://gitlab.com/gl-retrospectives/package/-/issues/30).
1. [ ] Review and comment on [Drive the Container Registry migration Phase 2 from Rails (&7316)](https://gitlab.com/groups/gitlab-org/-/epics/7316).
1. [ ] Review and comment on [DRAFT: Milestone 14.8 review and discussion (#345741)](https://gitlab.com/gitlab-org/gitlab/-/issues/345741).
1. [ ] Look at GitLab To-dos, and reduce the list to 30 items 😨 (currently at around 50).

#### Retrospective

- 🎯 I had personal stuff to do during my OOO time, but also was able to get some needed rest 😴.
- 👍 On call shift went smoothly with no incidents as expected, and I was able to use that time for some catching up after OOO time.
- 👍 12-27 was F&F Day, but I spent some time working on [!77212](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212) to compensate for wrapping up early on 12-24 (wasn't in the mood for working during the holiday 😬).

#### 2021-12-27

1. [x] Work on [Draft: Add package file cleanup jobs (!77212)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212). Address a couple pending TODOs to have it ready for pre-review. **Note**: MR 1 needs to be merged first so that I can rebase, fix conflicts and have it ready for review.
   - Investigated [failing `db:check-schema` job](https://gitlab.com/gitlab-org/gitlab/-/jobs/1918864376) in the MR pipeline. Tried re-applying the DB migrations I've added to see if that fixed the problem. I looked in the docs ([here](https://docs.gitlab.com/ee/development/database_review.html#preparation-when-adding-migrations) and [here](https://docs.gitlab.com/ee/development/migration_style_guide.html#schema-changes)), and [asked in `#database`](https://gitlab.slack.com/archives/C3NBYFJ6N/p1640641689015100) looking for clues. After a few hours I saw the pipeline was fixed. So, it seems the problem was the order of the constraints in `structure.sql`. I'm adding 1 column and 1 constraint, and originally I had moved the constraint to a different place that made some sense logically. I didn't know I had to respect the order for everything 😬.
   - Addressed a couple `TODO`s. I've added more robust error handling / status tracking. This created one additional `TODO` and one possible refactoring.

#### 2021-12-28

- OOO. Only checking Slack and email.

#### 2021-12-29

- OOO.

#### 2021-12-30

- OOO.

#### 2021-12-31

- [x] On-call shift from 9 to 13 MST.
- [x] Catch up on Slack and email.
  - Caught up on Slack and most emails. I wasn't able to look at a couple new issues/notifications from email. Will leave that for later.
  - **Highlight**: Looked at Kathy's [revisions to package cleanup policies after testing](https://gitlab.slack.com/archives/CAGEWDLPQ/p1640920180064600) (Slack message expiring around March 31), and provided some feedback.
- [ ] Resume training issues or work on stretch goals.
- [x] OOO (4 hrs).

### Week 53 (January 3)

#### Weekly Focus

- Take care of [Add package file status attribute and introduce the installable scope (!76767)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767/diffs#note_789976576) as David is OOO this week.
- Continue working on [!77212](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212) if possible, as this is dependent on [!76767](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767/diffs#note_789976576).

#### <a id="week-53-goal"></a>Weekly Goal

- Address pending feedback and have [Add package file status attribute and introduce the installable scope (!76767)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767/diffs#note_789976576) merged.

#### <a id="week-53-stretch-goals"></a>Stretch Goals

These are lower priority. I may as well address other incoming tasks that are more urgent instead of these.

1. [x] Review and comment on [14.6 Package retrospective (#30)](https://gitlab.com/gl-retrospectives/package/-/issues/30).
1. [ ] Review and comment on [Drive the Container Registry migration Phase 2 from Rails (&7316)](https://gitlab.com/groups/gitlab-org/-/epics/7316).
1. [x] Review and comment on [DRAFT: Milestone 14.8 review and discussion (#345741)](https://gitlab.com/gitlab-org/gitlab/-/issues/345741).
1. [ ] Look at GitLab To-dos, and reduce the list to 30 items 😨 (currently at around 54).

#### Retrospective

- 🎯 We were able to have [Add package file status attribute and introduce the installable scope (!76767)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767/diffs#note_789976576) merged. The pending review feedback that needed to be addressed were only a couple minor changes in the end.
- 👍 At the start of the week, pending catch-up wasn't so bad. So, I made some time to review and comment on a couple issues that were on my [stretch goals](#week-53-stretch-goals) radar for a while.
- 👍 Feeling good with the progress on [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755). Only hoping the pending reviews for MR 2 ([Add package file cleanup jobs (!77212)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212)) will go smoothly next week 🤞.
- 👍 With some effort, I made some time to review a [Package MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77518) that was more in the frontend camp 🙂.
- 👎 While focusing on my highest priority ([#345755](https://gitlab.com/gitlab-org/gitlab/-/issues/345755)) and considering urgency of my different work items, a couple days I've felt I'm not getting as much work done as I'd like. In those days, I find myself postponing a couple of those pending work items. The urgency of postponed items increases with each passing day. So, I worry about eventually reaching a point where I need to complete several very urgent items at the same time 😕.
- 📝 Learned about publishing generic packages and package files while testing [!76767](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767#camera-screenshots-or-screen-recordings) and [!77212](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212#how-to-set-up-and-validate-locally).
- 📝 Learned more about [database reviews in MRs](https://docs.gitlab.com/ee/development/database_review.html), and did my first DB review preparation in [!77212](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212#database-review).
- 📝 Learned about ways to test / troubleshoot Sidekiq jobs locally, also while [testing !77212](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212#how-to-set-up-and-validate-locally).

#### 2022-01-03

- [x] Family & Friends Day.

#### 2022-01-04

- [x] Finish catching up on emails.
- [ ] ~~1:1 with Michelle~~ Will reschedule because of Internet connection outage.
- [x] Pings, including some older ones from stretch goals.
  - [x] Review and comment on [14.6 Package retrospective (#30)](https://gitlab.com/gl-retrospectives/package/-/issues/30).
  - [x] Review and comment on [DRAFT: Milestone 14.8 review and discussion (#345741)](https://gitlab.com/gitlab-org/gitlab/-/issues/345741).
- [x] Quad planning.
- [ ] See latest review comments on [Add package file status attribute and introduce the installable scope (!76767)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767/diffs#note_789976576), and start working to resolve pending threads and address any new feedback.
  - Needed more time than expected to finish catching up and responding to older pings. So, I wasn't able to start working on this yet 😕.
- [x] Sync with Tim.

#### 2022-01-05

- [x] Pings.
- [x] 1:1 with Michelle.
- [x] See latest review comments on [Add package file status attribute and introduce the installable scope (!76767)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767/diffs#note_789976576), and work to resolve pending threads.
  - Spent some time looking at all the DB review comments and learning more about DB reviewing in MRs. In the end, the couple comments that needed to be addressed were actually very minor changes. So, I did those changes, rebased master, and tested locally. The DB reviewing learning should come in handy on MR 2 for this issue 👌.
- [x] Package weekly retro.
- [x] If time, review [Graphql: Add package managers api paths to details type (!77518)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77518).
  - After thinking if I should decline reviewing (due to urgency of corrective actions work), I decided to go ahead with it as this is a small MR. I spent at most 1 hr. on it (mostly because I tried to get a bit of context).
- [x] **Unplanned**: Add [weekly update for Confidential corrective actions issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/6044#note_802502175).

#### 2022-01-06

- [x] Pings.
- [x] Sync with Steve.
- [x] Continue working on [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755). Follow up on MR 1 and resume work on MR 2.
  - [!76767](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767) (MR 1) was maintainer approved and merged. So, I looked at how the FF should be [rolled out](https://gitlab.com/gitlab-org/gitlab/-/issues/348677).
  - I resumed work on [Draft: Add package file cleanup jobs (!77212)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212) (MR 2). Rebased `master` to include changes from MR 1, removed dummy code, addressed a couple pending `TODO`s, did a bit of tweaking, and filled the MR description. I'm still to add the query explain plans for the DB review, but with this I've moved the MR to `ready for review` and requested the initial backend review.
- [ ] Verify 14.7 MR of [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492) on Staging ([QA issue](https://gitlab.com/gitlab-org/release/tasks/-/issues/3304#grouppackage-grouppackage)).
- [x] Do another review pass for [Graphql: Add package managers api paths to details type (!77518)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77518).
  - Received a bit more clarification and approved 👍.

#### 2022-01-07

- [x] Pings.
- [ ] Verify [14.7 MR](https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/2028) of [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492) on Staging ([QA issue](https://gitlab.com/gitlab-org/release/tasks/-/issues/3304#grouppackage-grouppackage)).
  - I revisited this MR (remembering how to test it), and I think this can't be easily verified in production environments because I would need to change an Admin setting to test. The difficulties are: 1) I don't have admin access, and 2) even if I had or if I updated that setting via [Rails console](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/uncategorized/staging-environment.md#run-a-rails-console-in-staging-environment) (which I believe may be only intended for on-call incidents), I'm not sure if **I should** 🤔.
  - I also learned that the security release's MRs status is being tracked in other two issues besides the current security release issue: [Confidential release #3301](https://gitlab.com/gitlab-org/release/tasks/-/issues/3301#note_801995615) and [Confidential appsec #227](https://gitlab.com/gitlab-com/gl-security/appsec/appsec-team/-/issues/227).
- [ ] Work on [Create a background job for package files cleanup (#345755)](https://gitlab.com/gitlab-org/gitlab/-/issues/345755).
  - [x] Work on [Add package file cleanup jobs (!77212)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212). Finish preparation for DB review (add query explain plans), and address any backend review feedback.
    - The DB review preparation took me more time than expected as this is the first time I'm following this process. I also completed the instructions to set up and validate locally after initially having problems testing locally myself (enqueued jobs wouldn't be run by the GDK `rails-background-jobs` service). I looked at backend review feedback, but haven't addressed it yet. There are a couple refactoring ideas that I have asked David to give his thoughts on (kind of a maintainer pre-review) to possibly save time on implementation back-and-forth.
  - [ ] Begin the [feature flag rollout](https://gitlab.com/gitlab-org/gitlab/-/issues/348677) of [!76767](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767) (MR 1). This has now been deployed to `gprd`, `gprd-cny`, `gstg`, `gstg-cny`.
    - I've had no time for this yet 😕.

### Week 54 (January 10)

#### Weekly Focus

- [Add package file cleanup jobs (!77212)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212) (MR 2 of [#345755](https://gitlab.com/gitlab-org/gitlab/-/issues/345755)).
- [[Feature flag] Rollout of `packages_installable_package_files` (#348677)](https://gitlab.com/gitlab-org/gitlab/-/issues/348677).
- Following up on delivery of [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492) as part of the security release happening on Jan 11.

#### <a id="week-54-goal"></a>Weekly Goal

🎯 Have [Add package file cleanup jobs (!77212)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212) released, resulting in its issue ([#345755](https://gitlab.com/gitlab-org/gitlab/-/issues/345755)) delivered in the current milestone.

#### <a id="week-54-stretch-goals"></a>Stretch Goals

These are lower priority. I may as well address other incoming tasks that are more urgent instead of these.

1. [ ] Review and comment on [Drive the Container Registry migration Phase 2 from Rails (&7316)](https://gitlab.com/groups/gitlab-org/-/epics/7316).
1. [ ] Review and comment on [Expose deduplicated size of individual image repositories through the API (#347349)](https://gitlab.com/gitlab-org/gitlab/-/issues/347349#note_805653613).

#### Retrospective

- 👍 [!77212](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212) was practically finished at the beginning of the week.
- 👎 **But** reviewers were slow to respond in that MR 😔.
- 👍 I had my [security release delivered issue]([released](https://gitlab.com/gitlab-org/gitlab/-/issues/297492#note_807715372)).
- 👎 With my focus on delivering [!77212](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212), I kept leaving the [feature flag rollout](https://gitlab.com/gitlab-org/gitlab/-/issues/348677) of [!76767](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767) on the side, but fortunately David came back and took care of it without problems 🙌 🙇.
- 👍 I had some time for training issues due this month: [Gitlab Psychological Safety Training](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/35), and [Technical Writing Training](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/41).

#### 2022-01-10

- [x] Pings.
- [x] 1:1 with Michelle.
- [x] Work on [Add package file cleanup jobs (!77212)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212) (MR 2 of [#345755](https://gitlab.com/gitlab-org/gitlab/-/issues/345755)). Proceed with refactoring from [backend review feedback](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212#note_804696582), and follow up on the DB review (pending at the moment).
  - There was no DB review yet, but I completed the refactoring and assigned another backend reviewer to cover for Steve now being OOO.
- [ ] Begin the [feature flag rollout](https://gitlab.com/gitlab-org/gitlab/-/issues/348677) of [!76767](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/76767) (MR 1 of [#345755](https://gitlab.com/gitlab-org/gitlab/-/issues/345755)). This is still pending. The good thing is last week I talked about it with Steve and the rollout may be a matter of hours and not days in this case.
  - I kept leaving this on the side while focusing on MR 2, but fortunately David has taken care of it 🙌 🙇.
- [x] Add weekly update for [Confidential corrective actions issue](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/6044#note_802502175).
- [x] Weekly sync.

#### 2022-01-11

- [x] Pings.
- [x] Work on [Add package file cleanup jobs (!77212)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212) (MR 2 of [#345755](https://gitlab.com/gitlab-org/gitlab/-/issues/345755)). Address comments from new backend reviewer and initial comments from DB reviewer.
  - Looked at DB reviewer comments. They were non-blocking and the reviewer moved the MR to DB maintainer review. Addressed backend reviewer comments and requested a second pass.
- [x] Weekly quad planning.
- [x] **Unplanned**: I had some time available. So, I completed the [Gitlab Psychological Safety Training](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/35).
- [x] Follow up on delivery of [Confidential #297492](https://gitlab.com/gitlab-org/gitlab/-/issues/297492) on today's security release.
  - It has been [released](https://gitlab.com/gitlab-org/gitlab/-/issues/297492#note_807715372) 🎉.

#### 2022-01-12

- [x] Pings.
- [x] Work on [Add package file cleanup jobs (!77212)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212). Address [Danger warnings](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212#note_792134755) and request backend maintainer review. Follow up on DB maintainer review; may re-ping the reviewer if no response.
  - Rebased to fix all the warnings except for [MR size](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212#note_808538464), and requested backend maintainer review. Also pinged DB maintainer.
- [x] Weekly retro.
- [x] If time, start [Technical Writing Training](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/41).
  - Had some progress. Completed the Technical Writing One course.
  - **Note**: Will need to resume from [card 2/7](]https://gitlab.edcast.com/pathways/technical-writing-fundamentals/cards/5755268).

#### 2022-01-13

- [ ] Pings.
- [ ] ~~Sync with David.~~ I cancelled as I'll probably start my workday later today / need a non-linear workday.
- [ ] Work on [Add package file cleanup jobs (!77212)](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/77212). Address database maintainer and backend maintainer review feedback.
- [ ] If time, continue [Technical Writing Training](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/41).

#### 2022-01-14

- [ ] Pings.
- [ ] Sync with Michelle.

### Backlog

#### High Priority

1. [ ] Complete [Technical Writing Training](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/41) before [end of January]((https://gitlab.slack.com/archives/CAGEWDLPQ/p1639399955377600?thread_ts=1639391832.376800&cid=CAGEWDLPQ)).

#### Medium Priority

1. [ ] Review [Tips and tricks to be productive (#46)](https://gitlab.com/gitlab-org/ci-cd/package-stage/package/-/issues/46).
1. [ ] Look at GitLab To-dos, and reduce the list to 30 items 😨 (currently at around 54).
1. [ ] Set up new account for stock grants. I have postponed this as I see it as too distant in the future, but better take care of it and take it out of the radar until end of June 2022 🤞.
1. [ ] Investigate existing `pre-push` git hook that seems to be required for Git LFS. Does somebody else has that on their `gitlab-org/gitlab` git clones? The `security-harness` script throws a warning about it:
   >gitlab/.git/hooks/pre-push exists and is different from our hook!
   >Remove it and re-run this script to continue.
   - I've found an old [issue describing a similar problem](https://gitlab.com/gitlab-org/gitlab/-/issues/200022) where there was a conflict with a `pre-push` hook addded by [`overcommit`](https://github.com/sds/overcommit).
   - Currently, our [development stye guides](https://docs.gitlab.com/ee/development/contributing/style_guides.html#pre-push-static-analysis-with-lefthook) mention setting up [Lefthook](https://github.com/Arkweid/lefthook) for pre-push static analysis (it has replaced `overcommit`). So, I'm curious if `security-harness` wouldn't also have problems with it (I've not set up Lefthook yet). From looking at `lefthook.yml`, it would also use a pre-push hook, but maybe it does it in a different way and it doesn't use the `.git/hooks/pre-push` file?
   - For now, I've backed up the existing hook as `.git/hooks/pre-push.old`.
   - Apparently, Git LFS is setup in Git config. My `.git/config` file contains this 🤔:

     ```txt
     [lfs "https://gitlab.com/gitlab-org/gitlab.git/info/lfs"]
       access = basic
     [lfs]
     repositoryformatversion = 0
     ```

   - **Update**: After a recent `gdk update`, Lefthook has been installed automatically in my local environment. Not sure if this has removed the `pre-push` git hook that seems to be required for Git LFS. Not sure if this will cause any warnings next time I use the `security-harness` script.

1. [ ] On next iteration of trying to test Snowplow events, ask Tim to create a new Sisense dashboard, copying all the "Push and Pull" charts from the Package GitLab.com Stage Activity Dashboard and editing the queries to target Staging (`where GSC_ENVIRONMENT = 'staging'`).
1. [ ] Tweak [GitLab Package Registry administration](https://docs.gitlab.com/ee/administration/packages/) docs page.
   - Remove unnecessary text: "The Packages feature allows GitLab to act as a repository for the following:".
   - Update Debian Status link. It should be either [&6057](https://gitlab.com/groups/gitlab-org/-/epics/6057) (best on status) or [#5835](https://gitlab.com/gitlab-org/gitlab/-/issues/5835) (best on number of participants).
1. [ ] It would be nice to schedule a coffee-chat with a developer that joined GitLab around the same time as me so that we can share and compare our experience.
1. [ ] Have a look at [Package Registry demos and speedruns](https://about.gitlab.com/handbook/engineering/development/ops/package/#package-registry).
1. [ ] Create an MR to improve the [npm packages in the Package Registry](https://docs.gitlab.com/ee/user/packages/npm_registry/#authenticate-to-the-package-registry) docs page. Follow the [Documentation Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide/index.html). Link the MR to the [Make the Package Registry docs easier to follow (#340455)](https://gitlab.com/gitlab-org/gitlab/-/issues/340455) issue.

#### Low Priority

1. [ ] Get more familiar with Grafana and the [stage-groups: Group dashboard: package (Package)](https://dashboards.gitlab.net/d/stage-groups-package/stage-groups-group-dashboard-package-package?orgId=1) dashboard.
1. [ ] Create an MR to propose a preference for `include_examples` instead of `it_behaves_like` when it makes sense ([MR thread with discussion](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68054?commit_id=99b9b2634f5b41539eb618b421a0c89febe83142#note_671847965)).
1. [ ] Talk to Sofia about end-to-end testing.
   - What's the approach to run the Package tests on the different environments?
   - How's the data seeded? Do we spin up environments from scratch or do we use already seeded environments?
1. [ ] Gain context about virtual package registries (items [shared by David](https://gitlab.slack.com/archives/CAGEWDLPQ/p1633336449476700)):
   - [Epic](https://gitlab.com/groups/gitlab-org/-/epics/2614)
   - [Technical investigation](https://gitlab.com/gitlab-org/gitlab/-/issues/9164#note_345030994)
   - [Virtual registries postponed](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/76412)
1. [ ] Add myself as a backend code reviewer (example: <https://gitlab.com/gitlab-com/www-gitlab-com/blob/c895d1c7/data/team_members/person/l/luke-duncalfe.yml#L17-18>)

#### [Package onboarding](https://gitlab.com/gitlab-org/ci-cd/package-stage/onboarding/-/issues/5)

1. Learn more about the Package group, development workflows and practices by reading the handbook and development docs:
   - [ ] Read [Packages Development](https://docs.gitlab.com/ee/development/packages.html) in GitLab dev docs.
   - [ ] Read [doc/api/packages/npm.md](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/api/packages/npm.md) in gitlab-org/gitlab project.
1. [ ] Have a look at [Hugo Ortiz Senior Backend Engineer Role Entitlements (#10375)](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/10375)

#### [General onboarding](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/2845)

1. [ ] Add README (and scratch this from onboarding issue).
   >Consider creating and adding your own [README](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/#creating-and-publishing-your-gitlab-readme). This may take some time to create so please feel free to return to this task and complete it at a later date.

#### Local Dev Environment

1. [x] Tweak file watching config in VS Code
   - Had to follow the [VS Code instructions to fix this at the OS config level](https://code.visualstudio.com/docs/setup/linux#_visual-studio-code-is-unable-to-watch-for-file-changes-in-this-large-workspace-error-enospc). Excluding files via VS Code settings is not useful since the appropriate files are already excluded via `.gitignore`.
1. [ ] Create a git bare repository to save my .dotfiles in a GitLab project. Make sure to include: `~/.git_commit_msg.txt`.
   - [How to store dotfiles | Atlassian Git Tutorial](https://www.atlassian.com/git/tutorials/dotfiles "How to store dotfiles | Atlassian Git Tutorial")
   - [maxprehl/dotfiles: Git bare repo collection of my dotfiles.](https://github.com/maxprehl/dotfiles "maxprehl/dotfiles: Git bare repo collection of my dotfiles.")
   - [Managing dotfiles with a bare git repo](https://marcel.is/managing-dotfiles-with-git-bare-repo/ "Managing dotfiles with a bare git repo")
   - [Ask HN: What do you use to manage dotfiles? | Hacker News](https://news.ycombinator.com/item?id=11070797 "Ask HN: What do you use to manage dotfiles? | Hacker News")
   - [GitHub does dotfiles - dotfiles.github.io](https://dotfiles.github.io/ "GitHub does dotfiles - dotfiles.github.io")
1. [ ] Use [`git-config` conditional includes](https://git-scm.com/docs/git-config#_conditional_includes) for diferent configurations for work and personal projects.
   - [Brief tutorial on this](https://www.freecodecamp.org/news/how-to-handle-multiple-git-configurations-in-one-machine/).
   - [Another tutorial](https://alysivji.github.io/multiple-gitconfig-files.html)
1. [ ] Find best approach to linting `*.md.erb` files (used in www-gitlab-com) locally:
   - Use [`format-markdown`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/doc/formatting_markdown.md).
   - Find how to use the `markdownlint` VS Code extension for *.md.erb files.
   - Do manual linting using [`markdownlint-cli2`](https://github.com/DavidAnson/markdownlint-cli2).
1. [ ] Try [GLab](https://github.com/profclems/glab) GitLab CLI tool.
1. [ ] Try [GitLab VS Code Extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension).

#### This README

1. [ ] Add information in README
   - [Creating and publishing your GitLab README](https://about.gitlab.com/handbook/marketing/inbound-marketing/digital-experience/website/#creating-and-publishing-your-gitlab-readme)
   - [GitLab Engineering READMEs](https://about.gitlab.com/handbook/engineering/readmes/)
   - [RichardLitt/standard-readme: A standard style for README files](https://github.com/RichardLitt/standard-readme)
   - [12 “Manager READMEs” from Silicon Valley’s Top Tech Companies](https://hackernoon.com/12-manager-readmes-from-silicon-valleys-top-tech-companies-26588a660afe)

#### Other

1. Fix issues in README for <https://gitlab.com/gitlab-com/teampage-map>:
   1. [ ] Fix `data/team.yml` broken link — File was removed from source control in [#10507](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10507) and is now built on deployment and exposed under <https://about.gitlab.com/company/team/team.yml>.
   1. [ ] Fix typos and Markdown formatting.

### Reference

#### Package Event Tracking / Developing and Testing Snowplow

- [Snowplow Guide | GitLab](https://docs.gitlab.com/ee/development/snowplow/index.html#snowplow-micro)
- [Testing best practices | GitLab](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html#test-snowplow-events)
- [Adds package event tracking (!41846) · Merge requests · GitLab.org / GitLab · GitLab](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/41846/diffs)
- [Snowplow - YouTube](https://www.youtube.com/playlist?list=PL0ngvSPSoo_kRE7Ky42FVFtQjcDvfTcHA)
- [Snowplow Analytics workshop 2021-04-28 - Google Slides](https://docs.google.com/presentation/d/16FlzwToS6ps8X7HFtoeVeumZI6TICLnzxmccg7VZlXE/edit#slide=id.g29a70c6c35_0_68)
- [Product Intelligence - Snowplow 2.0 Workshop - Google Docs](https://docs.google.com/document/d/1orA6THJHAdESax8mGCWKxs0Xjz4ySpWCkZ3WU78VLZU/edit#heading=h.feafdf5bjqdy)
- [Understanding the structure of Snowplow data - Snowplow Docs](https://docs.snowplowanalytics.com/docs/understanding-your-pipeline/canonical-event/#Custom_structured_events)
- [Initialization - Snowplow Docs](https://docs.snowplowanalytics.com/docs/collecting-data/collecting-from-own-applications/ruby-tracker/initialization/)
- [snowplow/snowplow-ruby-tracker: Snowplow event tracker for Ruby. Add analytics to your Ruby and Rails apps and gems](https://github.com/snowplow/snowplow-ruby-tracker)
- [What is Snowplow Micro? - Snowplow Docs](https://docs.snowplowanalytics.com/docs/understanding-your-pipeline/what-is-snowplow-micro/)
- [snowplow-incubator/snowplow-micro: Standalone application to automate testing of trackers](https://github.com/snowplow-incubator/snowplow-micro/tree/master)
- [GitLab dbt Docs](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.snowplow_structured_events_all)
- [Package: User Adoption and Growth - Sisense for Cloud Data Teams](https://app.periscopedata.com/app/gitlab/805350/Package:-User-Adoption-and-Growth)

#### Product Data

- [Data For Product Managers | GitLab](https://about.gitlab.com/handbook/business-technology/data-team/programs/data-for-product-managers/#snowplow)
- [Sisense For Cloud Data Teams‎ | GitLab](https://about.gitlab.com/handbook/business-technology/data-team/platform/periscope/#self-service-dashboard-development)
- [Periscope Editor Training.mp4 - Google Drive](https://drive.google.com/file/d/15tm_zomS2Ny6NdWiUNJlZ0_73THDiDww/view)
- [Package GitLab.com Stage Activity Dashboard - Sisense for Cloud Data Teams](https://app.periscopedata.com/app/gitlab/527857/Package-GitLab.com-Stage-Activity-Dashboard)
- [Structured Event : count, last 30 days - Snowplow Event Exploration - last 30 days - Sisense for Cloud Data Teams](https://app.periscopedata.com/app/gitlab/539181/Snowplow-Event-Exploration---last-30-days?widget=6989504&udv=0)
- [GitLab dbt Docs](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.snowplow_structured_events_all)
- [GitLab dbt Docs](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.snowplow_staging_unnested_events_30)

#### Ruby / Rails

- [Rack HTTP status codes](https://github.com/rack/rack/blob/f2d2df4016a906beec755b63b4edfcc07b58ee05/lib/rack/utils.rb#L490)
